#!/bin/bash

# tiddlywiki doesn't allow extending boot js
# without copying the code into the node_module.
# hence this script.

bootscript=tiddlers/asekai-datafile/_boot_boot.js
bootdir=node_modules/tiddlywiki/boot

if [[ ! -f "$bootdir/boot.js.bak" ]]; then
  mv "$bootdir/boot.js" "$bootdir/boot.js.bak"
  cp "$bootscript" "$bootdir/boot.js"
  echo "tiddlywiki/boot.js was jury-rigged successfully"
fi
