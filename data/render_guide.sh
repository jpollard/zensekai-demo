#!/bin/bash

tiddlywiki=./node_modules/tiddlywiki/tiddlywiki.js
mode="$1"
node=`which node`

# so this script can be run in jedit's console
if [ -z "$node" ]; then
node=~/.nvm/versions/node/v10.16.0/bin/node
fi

main() {
if [ "$mode" == "full" ]; then
  pageselector
  rm "../guide/paging.css"
  mv "./output/paging.css" "../guide/"
fi
# render main guide file
echo "- rendering main kai guide"
$node $tiddlywiki --render "guidetest.html" "[addsuffix[]]" "text/plain" "guide_generator/master"
echo "- main guide finished"
# move to "guide"
rm "../guide/guidetest.html"
mv "./output/guidetest.html" "../guide/"
}

# render page selector & paging stylesheet
pageselector(){
pagername=./data_output/tiddlers/guide_cache_pageselector.tid
echo "rendering paging style"
$node $tiddlywiki --render "paging.css" "[addsuffix[]]" "text/plain" "guide_generator/pagingstyle"
echo "rendering page selector"
$node $tiddlywiki --render "guide_generator/pageselector" "[removeprefix[guide_generator/]addprefix[/tmp/asekai/]]" \
"text/plain" "$:/core/templates/html-tiddler"

echo -e "title: $:/asekai/cache/guide/page-selector\n" > $pagername

# tiddlywiki's automatic <p> around everything is annoying normally, but fatal for the page selector
# so they must be removed with sed. tee -a ... > null appends to a file with no echo
sed 's/<\/\?p>//g' /tmp/asekai/pageselector | tee -a $pagername > /dev/null
}

main
