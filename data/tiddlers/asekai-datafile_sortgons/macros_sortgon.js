/*\
jedit: :folding=explicit:
created: 20190626025843013
tags: $:/asekai/datafile/macros
title: $:/asekai/datafile/macros/sortgon
type: application/javascript
module-type: macro

sortgon - TODO: make sure this is the updated version

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var util = require('$:/asekai/datafile/util.js').util;

/* Information about this macro */
exports.name = "sortgon";
exports.params = [ {name: "sector"}, {name: "shift"}, {name: "phase"}, {name: "sort"}, {name: "kai"} ];


const alignBars = {
	shift: {
		1: 8, 2: 7, 3: 6,
		4: 5, 5: 3, 4.5: 4,
		6: 2, 7: 1, 8: 0
	},
	
	phase: {
		1: 0, 2: 1, 3: 2,
		4: 3, 5: 5, 4.5: 4,
		6: 6, 7: 7, 8: 8
	}
};

var shiftBar = function(num) {
	var table = alignBars['shift'];
	return table[num];
};

var phaseBar = function(num) {
	var table = alignBars['phase'];
	return table[num];
};

//console.log(shiftBar(1),shiftBar(2),shiftBar(3),shiftBar(4),shiftBar(4.5),shiftBar(5),shiftBar(6),shiftBar(7),shiftBar(8));


/*
Run the macro
*/
exports.run = function(sector, shift, phase, sort, kai) {
	var result, svgInitial, svgNodes,
	    sortBkg, shiftPath, phasePath, shiftPath2, phasePath2, shiftI, phaseI, sectorName;
	const sectors = {
	  1: 'nature', 2: 'culture', 3: 'spirit', 4: 'physics',
	  "1a": 'freeze', "2a": 'burn', "3a": 'stain', "4a": 'purge'
	},
	sortgonPrefix = '$:/asekai/datafile/sortgons/';
	
	const DEFAULT_SECTOR = 4;


	if (kai !== undefined) {
		kai = util.identFull(kai);
		
		sector = this.wiki.getTextReference('k_' + kai + '!!num-sector', DEFAULT_SECTOR);
		shift = this.wiki.getTextReference('k_' + kai + '!!align-shift', 4.5) * 1;
		phase = this.wiki.getTextReference('k_' + kai + '!!align-phase', 4.5) * 1;
		sort = this.wiki.getTextReference('k_' + kai + '!!align-sort', 'core');
	}                           

	sectorName = sectors[sector];
	svgInitial = this.wiki.getTextReference(sortgonPrefix + 'sortgon_' + sectorName + '.svg', null);

	shift = (isNaN(shift) ? 4.5 : shift);
	phase = (isNaN(phase) ? 4.5 : phase);
	
	if (svgInitial === null) {
		sectorName = sectors[DEFAULT_SECTOR];
		svgInitial = this.wiki.getTextReference(sortgonPrefix + 'sortgon_' + sectorName + '.svg', null);
	}
	
	//onsole.log('sortgon debug', kai, sector, shift, phase, sort);
	
	
	try {

	svgNodes = new DOMParser().parseFromString(svgInitial, "text/html");
	sortBkg = svgNodes.querySelector('.sort_bkg');
	shiftPath = svgNodes.querySelector('.shift_path');
	phasePath = svgNodes.querySelector('.phase_path');
	shiftPath2 = svgNodes.querySelector('.shift_path2');
	phasePath2 = svgNodes.querySelector('.phase_path2');

	if (sort) {
		sortBkg.setAttribute('class', 'sortgon_path thin sort_bkg sort_' + sort);
	}

	shiftI = shiftBar(shift);
	phaseI = phaseBar(phase);

	shiftPath.setAttribute('clip-path', 'url(#clip-' + sectorName + '-dn-' + shiftI + ')');
	phasePath.setAttribute('clip-path', 'url(#clip-' + sectorName + '-ce-' + phaseI + ')');

	// if shift is 8 or phase is 8, hide the path
	if (shiftI == 0) {
		shiftPath.setAttribute('style', 'display: none;');
	}
	if (phaseI == 0) {
		phasePath.setAttribute('style', 'display: none;');
	}

	//darken non-sort part

	if (shift < 4.5) {
		shiftPath2.setAttribute('class', 'sortgon_path thick shift_path2 dark');
	}
	else if (shift > 4.5) {
		shiftPath.setAttribute('class', 'sortgon_path thick shift_path dark');
	}
	else if (sort) {
		if (/d[pf]/.test(sort)) {
			shiftPath2.setAttribute('class', 'sortgon_path thick shift_path2 dark');
		}
		else if (/n[pf]/.test(sort)) {
			shiftPath.setAttribute('class', 'sortgon_path thick shift_path dark');
		}
	}

	if (phase < 4.5) {
		phasePath.setAttribute('class', 'sortgon_path thick phase_path dark');
	}
	else if (phase > 4.5) {
		phasePath2.setAttribute('class', 'sortgon_path thick phase_path2 dark');
	}
	else if (sort) {
		if (/c[pf]/.test(sort)) {
			phasePath.setAttribute('class', 'sortgon_path thick phase_path dark');
		}
		else if (/e[pf]/.test(sort)) {
			phasePath2.setAttribute('class', 'sortgon_path thick phase_path2 dark');
		}
	}


	}
	catch (e) {
		console.log(e);
	}
	//console.log(shiftPath, phasePath);
	
	
	result = '<div style="min-height: 7em; width: 7em; border: 1px solid black;">' +
	svgNodes.body.innerHTML + '<span> s ' + shift + ' / p ' + phase + ' </span></div>';
	
	return result;
};

})();
