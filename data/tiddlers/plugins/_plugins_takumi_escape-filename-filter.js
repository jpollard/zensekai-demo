/*\
jedit: :folding=explicit:
created: 20190621031249598
tags: Plugins
title: $:/plugins/takumi/escape-filename-filter.js
type: application/javascript
module-type: filteroperator

escape filenames with underscores

\*/

(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

/*
Export our filter function
*/
exports.escapefilename = function(source,operator,options) {
	var results = [];
	source(function(tiddler,title) {
		if(title.substr(0,operator.operand.length) === operator.operand) {
			results.push(title.replace(/[\<\>:"\\\/\|\?\*\^]/g, '_'));
		}
	});
	return results;
};

})();
