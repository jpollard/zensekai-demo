/*\
title: $:/plugins/takumi/kai-ident
type: application/javascript
module-type: macro

return name of a kai tid

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

/*
Information about this macro
*/
exports.name = "kai-ident";
exports.params = [ {name: "tiddler"} ];


/*
Run the macro
*/
exports.run = function(tiddler) {
	if (/^k_/.test(tiddler) === true) {
		tiddler = tiddler.replace(/^k_/, '');
	}
	
	return tiddler;
};

})();
