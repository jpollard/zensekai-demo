/*\
jedit: :folding=explicit:
created: 20190316200219253
tags: $:/asekai/datafile
title: $:/asekai/datafile/lib/heart.js
type: application/javascript
module-type: library

heart.js

\*/
// requires etc {{{
/*jshint node: true, esnext: true, laxcomma: true */
/*global jedit: 'already' */

// heart.js
// - the new version of "fluff", a library which read from tiddlywiki datafiles
// this version can read from json (/msgpack) files, which should be faster
// this file contains only helper functions, and can be included in tiddlywiki.

// var foo = require bar;

(function(){
'use strict';

if (typeof $tw !== 'undefined') {
	var Hjson = require('$:/plugins/laktak/hjson/hjson.js');
}
else {
	var Hjson = require('hjson');
}

// TODO: cleanup later
var globals = {
	sectors: {
		"1": "nature",
		"2": "culture",
		"3": "spirit",
		"4": "physics",
		
		"1a": "freeze",
		"2a": "burn",
		"3a": "stain",
		"4a": "purge"
	},
	sectorNames: {
		"1": "Nature",
		"2": "Culture",
		"3": "Spirit",
		"4": "Physics",
		
		"1a": "Freeze",
		"2a": "Burn",
		"3a": "Stain",
		"4a": "Purge"
	}
};
// }}}


var heart = { // {{{
	
I_HATE_COMMAS: true


, sectorTag: function(num) {
	return globals.sectors[num];
}

, sectorName: function(num) {
	return globals.sectorNames[num];
}

	
, findPre: function(tid) {
	return tid.childNodes[1].innerHTML;
}

, tagString: function(array) {
	return '[[' + array.join(']] [[') + ']]';
}

, createTid: function(title, text, fields) {
	var div, pre, i;
	
	div = document.createElement('div');

	if (title) {
		div.setAttribute('title', title);
	}
	if (!text) {
		text = '';
	}

	for (i in fields) {
		if (i == 'tags' && typeof fields[i] == 'object') {
			div.setAttribute(i, this.tagString(fields[i]));
		}
		else if (i !== 'text') {
			div.setAttribute(i, fields[i]);
		}
	}

	div.innerHTML = '\n<pre>' + text + '</pre>\n';
	return div;
}

, getTidTexts: function(tids, callback) {
	var tids, tids2 = [], tidList = {}, i;
	
	if (callback === undefined || typeof callback !== 'function') {
		callback = function(input) {
			return input;
		};
	}
	
	for (i = 0; i < tids.length; i++) {
		if (tids[i].title !== undefined) {
			tids2[i] = tids[i];
		}
		else {
			tids2[i] = this.flattenAttributes(tids[i]);
		}
		
		if (tids2[i].text !== undefined) {
			tidList[ tids2[i].title ] = callback(tids2[i].text);
		}
		else {
			tidList[ tids2[i].title ] = '';
		}
	}
	
	return tidList;
}

, flattenAttributes: function(tid) {
	var dataObj = {}, j, domNodeAvailable = false;
	
	if (typeof tid == 'undefined' || tid === null) {
		return {};
	}
	
	// make sure "Node" class is available.
	// this cannot be done on the fly in strict mode
	if (typeof Node === "object") {
		domNodeAvailable = true;
	}
	
	if (domNodeAvailable === true && tid instanceof Node) {
		for (j = 0; j < tid.attributes.length; j++) {
			dataObj[ tid.attributes[j].name ] =
			tid.attributes[j].value;
		}
		
		if (tid.childNodes !== undefined && tid.childNodes[1] !== undefined) {
			dataObj.text = tid.childNodes[1].innerHTML;
		}
	}
	else if (tid.fields !== undefined) {
		dataObj = this.unfreeze(tid.fields);
	}
	
	return dataObj;
}

// "unfreeze" a tid object by turning it to a regular one
, unfreeze: function(tid) {
	var i, result;
	
	result = {};
	
	for (i in tid) {
		result[i] = tid[i];
	}
	
	return result;
}

, guideExcludeExp: function(configTid) { // {{{
	var noGuide, i;
	
	if (configTid && configTid.text !== undefined) {
		configTid = JSON.parse(configTid.text);
	
		if (configTid.noguide && configTid.noguide.length > 0) {
			noGuide = [];
			
			for (i = 0; i < configTid.noguide.length; i++) {
				if (/_/.test(configTid.noguide[i])) {
					noGuide.push(configTid.noguide[i] + '(\/[a-z]+)*');
				}
				else {
					noGuide.push(configTid.noguide[i] + '(_[a-z0-9]+)?(\/[a-z]+)*');
				}
			}
			
			if (noGuide.length > 0) {
				noGuide = new RegExp('(' + noGuide.join('|') + ')$');
			}
		}
		
		//onsole.log('noguide exp', noGuide);
	}
	
	return noGuide;
} // }}}

, parseNamePron: function(pron) { // {{{
	var pronArray;
	
	if (/[\[\]]/.test(pron) === false) {
		return pron;
	}
	
	try {
		//pron = pron.replace(/'/g, '"');
		//pron = pron.replace('","', '", "');
		pron = '{ "pron": ' + pron + ' }\n';
		//console.log('PRON DEBUG', pron);
		pron = this.hjsonParse(pron);
	}
	catch (e) {
		console.error('error parsing name pron', e);
		return 'ERROR';
	}
	
	pronArray = pron.pron;
	return pronArray;
	
} // }}}

// I casually call this hjson but it's actually regular javascript object notation
// with some of hjson's features, resulting in MORE room to be degenerate
// so.... things need to be cleaned a little before passing to hjson
, hjsonParse: function(hjson) { // {{{
	var result, json;
	
	result = hjson;
	
	// remove comments
	result = result.replace(/(\/\/(?![a-z]+\.[a-z0-9\-]+\.org)[^\n]*\n)/g, '\n');
	
	// deal with single quote strings
	//result = result.replace(/"(?!([^"\n]+")*[:,][\s\n]|([^"]*")?(\s?[\]\}])*\n)/g, '\\"');
	//result = result.replace(/(:[\s\n])'([^']+)'/g, '$1"$2"');
	
	// appease hjson
	result = result.replace(/:\n"/g, ': "');
	
	// add commas
	result = result.replace(/("|[0-9]+|[\}\]])[\s\t]+"/g, '$1,\n"');
	//result = result.replace(/([\}\]])[\s\t\n]+([\{\[])/g, '$1,\n$2');
	result = result.replace(/(:\n?[\{])\s*([a-z0-9]+:)/g, '$1\n$2');
	result = result.replace(/([a-z0-9]:+\s("[^"]+"|[0-9]+|true|false)|\})(?!;)\s?}/g, '$1\n}');
	//result = result.replace(/\s\}(?!")/g, '\n}');
		
	
	if (/^\n*\{/.test(result) !== true) {
		result = '{\n' + result + '\n}';
	}
	
	//onsole.log('HJSONPARSE', result);
	
	try {
		json = Hjson.parse(result);
	}
	catch (e) {
		console.error('error parsing metas tid', hjson, result)
		console.error(e.message);
		return {};
	}
	
	return json;
	
} // }}}

, cleanSprite: function(svg) {
	var nastyRegex = new RegExp('<'+'svg');
	
	if (/&[lg]t;/g.test(svg) === false) {
		return svg;
	}
	
	svg = svg.replace(/&lt;/g, '<');
	svg = svg.replace(/&gt;/g, '>');
	svg = svg.replace(/<\?xml[a-zA-Z0-9="\.\s-]+\?>[\s\n]+/, '');
	
	svg = svg.replace(nastyRegex, '<'+'svg class="sprite"');
	
	return svg;
}


, zerofill: function(no, places) {
	var i, result = no + '';
	
	if (result.length < places) {
		for (i = (places - result.length); i > 0; i--) {
			result = '0' + result;
		}
	}
	
	return result;
}

, dateString: function() { // 
	var string, date;
	
	date = new Date();
	
	//string = '' + date.getUTCFullYear() + '.' + (date.getUTCMonth() + 1) + '.' + date.getUTCDate() + '.' + date.getUTCHours() + '.' + date.getUTCMinutes() + '.' + date.getUTCSeconds() + '.' + date.getUTCMilliseconds();
	
	string = '' + date.getUTCFullYear() + '' + this.zerofill(date.getUTCMonth() + 1, 2) + '' + date.getUTCDate() + '' + this.zerofill(date.getUTCHours(), 2) + '' + date.getUTCMinutes() + '' + this.zerofill(date.getUTCSeconds(), 2) + '' + date.getUTCMilliseconds();
	
	//onsole.log('moddate', string);
	return string;
	
}





// output datafile as a tw plugin
// - to be used as test data (but real and current) when testing the template
, makeTWPlugin: function(core) {
	var plugin, result;
	
	// allow passing DataStore
	if (core.filetype !== undefined) {
		if (core.filetype == 'json') {
			plugin = { 'tiddlers': core.data };
			return JSON.stringify(plugin);
		}
		else if (core.filetype == 'html') {
			core = core.core;
		}
	}
	
	plugin = heart.makeJSON(core);
	result = JSON.stringify(plugin, null, 2);
	
	return result;
}

, makeJSONPacked: function(core) {
	var plugin, result;
	
	plugin = heart.makeJSON(core, true);
	result = JSON.stringify(plugin);
	
	return result;
}

// convert a tiddlywiki storeArea to json,
// for tiddlywiki plugin or reloading into heart.js later
, makeJSON: function(core, stripTW) { // {{{
	var kaiTids, i, j,
	    pluginObj = {}, pluginTid,
	    configTid, noGuide;
	
	//
	//onsole.log('MAKESSON', core, stripTW);
	
	if (core instanceof Node) {
		kaiTids = core.querySelectorAll('div');
		// '[title^="k_"]'
		configTid = this.flattenAttributes( core.querySelector('[title="$:/asekai/config"]') );
	}
	else {
		configTid = core['$:/asekai/config'];
		
		if (core.tiddlers !== undefined) {
			core = core.tiddlers;
		}
		
		kaiTids = [];
			
		for (i in core) {
			core[i].title = i;
			kaiTids.push(core[i]);
		}
		
		// allow this function to accept an entirely processed json output
		// in which case we simply spit it back out
		if (core['$:/asekai/datafile/data/readme'] !== undefined &&
			core['$:/asekai/datafile/data/readme'].minimal === true) {
			return core;
		}
	}
	
	// respect noguide
	noGuide = this.guideExcludeExp(configTid);
	
	
	for (i = 0; i < kaiTids.length; i++) {
		if (kaiTids[i] instanceof Node) {
			pluginTid = this.flattenAttributes(kaiTids[i]);
		}
		else {
			pluginTid = kaiTids[i];
		}
		
		if (/(^(p_|(Gen|Items|Quotes)\/)|(\/(strings|sprites|palettes)$))|\$:\/asekai\/config/.test(pluginTid.title) === true ||
		  (noGuide !== undefined && noGuide.test(pluginTid.title) === true) ) {
			continue;
		}
		// for now, org tags from datafile - k_entity/strings,sprites,palettes - are removed
		
		// if making a json version of datafile, clean up fields
		if (stripTW === true) {
			//onsole.log('PLUGINTID', pluginTid);
			
			for (j in pluginTid) {
				if (/created|modified|tags/.test(j) === true) {
					delete pluginTid[j];
				}
				// number
				else if ( /align-(shift|phase)|num-(sector|index|serial)/.test(j) === true && /^[0-9\.]+$/.test(pluginTid[j]) ) {
					pluginTid[j] = pluginTid[j] * 1;
					// sometimes this will result in "null" but......
					// that's more machine readable anyway so w/e
				}
				// boolean
				else if (/stage-kerv|num-rare|ability-(mystery|pieces)/.test(j) === true) {
					pluginTid[j] = (pluginTid[j] === "true" ? true : false);
				}
				else if (j == 'name-pron') {
					try {
						pluginTid[j] = this.parseNamePron(pluginTid[j]);
					}
					catch (e) {
						pluginTid[j] = pluginTid[j] + ' [ERROR]';
						console.error('error parsing name-pron of', pluginTid.title, pluginTid[j], e);
					}
				}
				else if (j == "text") {
					try {
						if (pluginTid.type == 'application/json') {
							pluginTid.text = JSON.parse(pluginTid.text);
						}
						else if (pluginTid.type == 'image/svg+xml') {
							pluginTid.text = this.cleanSprite(pluginTid.text);
							
							pluginTid.text = pluginTid.text.replace(/\n+\t*/g, ' ');
						}
					}
					catch (e) {
						console.error('error parsing tid', pluginTid.title, 'as', pluginTid.type, e);
					}
				}
			}
			
			// for a bit I was deleting type but it's better not to
		}
		else {
			if (/^k_[a-z]+$/.test(pluginTid.title) === true) {
				//onsole.log('plugintidtags', pluginTid.tags);
				
				// this shouldn't be a thing, tags should be space separated or array,
				// but, deal with it
				if (typeof pluginTid.tags == 'string' && /,/.test(pluginTid.tags)) {
					pluginTid.tags = pluginTid.tags.split(',');
				}
				
				if (pluginTid.tags instanceof Array) {
					pluginTid.tags = '[[' + pluginTid.tags.join(']] [[') + ']]';
				}
				
				// for a while I was adding organisation tags here
				// but now they're added by kai-strain plugin
			}
			// hide sub tids that are included
			else if (/^k_[a-z_]+(\/[a-z0-9]+)+$/.test(pluginTid.title) === true) {
				pluginTid.tags = '';
			}
		}
		
		// if text is empty, omit
		if (pluginTid.text == '') {
			delete pluginTid.text;
		}
		
		pluginObj[ pluginTid.title ] = pluginTid;
		delete pluginTid.title;
	}
	
	if (stripTW !== true) {
		pluginObj[ '$:/asekai/datafile/data/readme' ] = {
			text:
			"This plugin contains a cached copy of the data from the asekai datafile, for testing the tw kai guide template.\n\n" +
			"It was generated by make_kaiguide.js on " + (heart.dateString())
		};
		
		pluginObj = { tiddlers: pluginObj };
	}
	else {
		pluginObj[ '$:/asekai/datafile/data/readme' ] = {
			text: "",
			minimal: true
		};
	}
	
	//onsole.log('PLUGIN OBK', pluginObj);
	
	return pluginObj;
} // }}}

, dataPluginTid: function(plugin) {
	return this.createTid('$:/asekai/datafile/data', plugin, {
		author: "Takumi",
		"core-version": ">=5.0.0",
		"description": "Asekai datafile testing plugin",
		list: "readme",
		"plugin-type": "plugin",
		"type": "application/json",
		"version": "19.03.07",
		"created": "20190307092109772",
		"modified": heart.dateString()
	});
}


, escapeUnicode: function(character) {
	var num, i, escapes = [];
	
	for (i = 0; i < character.length; i++) {
		num = character.charCodeAt(i);
		
		if (Number.isNaN(num)) {
			throw Error('character code undefined for char', character);
		}
		
		escapes.push( '\\u' + (num.toString(16).padStart(4, '0')) );
	}

	return escapes.join('');
}




, Utf8ArrayToStr: function(array) { // {{{
// https://stackoverflow.com/questions/8936984
    var out, i, len, c,
        char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while(i < len) {
    c = array[i++];
    switch(c >> 4)
    { 
      case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
        // 0xxxxxxx
        out += String.fromCharCode(c);
        break;
      case 12: case 13:
        // 110x xxxx   10xx xxxx
        char2 = array[i++];
        out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
        break;
      case 14:
        // 1110 xxxx  10xx xxxx  10xx xxxx
        char2 = array[i++];
        char3 = array[i++];
        out += String.fromCharCode(((c & 0x0F) << 12) |
                       ((char2 & 0x3F) << 6) |
                       ((char3 & 0x3F) << 0));
        break;
    }
    }

    return out;
} // }}}



// funcs which should probably be in another file

, sectorNum: function(tid) {
	return this.zerofill(tid['num-sector'], 2) + '-' + this.zerofill(tid['num-index'], 4);
}

// TODO: get these from datafile, so they can be localised
, isUniqueAdj: function(adj) {
	var variants =
	[ 'Breeze', 'Millennium', 'Tall', 'Metric', 'World', 'Chrome', 'Liminal', 'Grit',
	'Youth', 'True' ];
	
	if (variants.indexOf(adj) === -1) {
		return true;
	}
	
	return false;
}

, stageSorting: function(stage) {
	var stages =
	['youth', 'subadult', 'basic', 'none', 'recurve', 'm1', 'm2', 'm3', 'm4', 'm5' ];
	
	if (stage === undefined || stages.indexOf(stage) === -1) {
		stage = 'none';
	}
	
	return this.zerofill(stages.indexOf(stage), 2);
}




}; // }}}

/*

var TiddlyWiki = {

I_HATE_COMMAS: true

, pad: function(value,length) {
	length = length || 2;
	var s = value.toString();
	if(s.length < length) {
		s = "000000000000000000000000000".substr(0,length - s.length) + s;
	}
	return s;
}

// Convert a date into UTC YYYYMMDDHHMMSSmmm format
, stringifyDate: function(value) {
	return value.getUTCFullYear() +
			$tw.utils.pad(value.getUTCMonth() + 1) +
			$tw.utils.pad(value.getUTCDate()) +
			$tw.utils.pad(value.getUTCHours()) +
			$tw.utils.pad(value.getUTCMinutes()) +
			$tw.utils.pad(value.getUTCSeconds()) +
			$tw.utils.pad(value.getUTCMilliseconds(),3);
}

}

*/


delete heart.I_HATE_COMMAS;
//var module = module;
module.exports = heart;

})();

