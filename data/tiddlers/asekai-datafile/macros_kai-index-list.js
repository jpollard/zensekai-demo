/*\
jedit: :folding=explicit:
title: $:/asekai/datafile/macros/kai-index-list.js
tags: $:/asekai/datafile/macros
type: application/javascript
module-type: macro

return a sector-specific kai index

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var kaiStrain = require('$:/asekai/datafile/startup/kai-strain.js');

/* Information about this macro */
exports.name = "kai-indexlist";
exports.params = [ {name: "sector"}, {name: "index"} ];


/* Run the macro */
exports.run = function(sector, index) {
	var indexTid, parsed, sectorIndex, list = [],
		i;
	const EMPTY = 'NONE';
	
	indexTid = $tw.wiki.getTiddler(index);
	
	try {
		indexTid = indexTid.fields.text;
		
		parsed = kaiStrain.parseStrainIndex(indexTid);
		sectorIndex = parsed.sectors[sector].list;
		
		//onsole.log("SECTOR INDEX", parsed.sectors[sector]);
		
		for (i = 0; i < sectorIndex.length; i++) {
			list.push( sectorIndex[i].replace(/^(.+)\/([0-9]+)$/, '$1') );
		}
		
		list = list.join(' ');
	}
	catch (e) {
		return "ERROR - sector &quot;" + sector + "&quot;";
	}
	
	return list;
	
};

})();