/*\
title: $:/core/modules/parsers/textparser.js
type: application/javascript
module-type: parser

The plain text parser processes blocks of source text into a degenerate parse tree consisting of a single text node

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";


var stringifySmall = function (key, value) {
var string;
if (typeof value !== 'object') {
return value;
}
else {
string = JSON.stringify(value);
if (string.length <= 60) {
return string;
}

return value;
}
};

// attempt to pretty print json
var parseJson = function(text) {
var obj, string;
try {
obj = JSON.parse(text);
string = JSON.stringify(obj, stringifySmall, 3);
string = string.replace(/"({[^}]+})"/g, '$1').replace(/\\"/g, '"');
return string;
}
catch (e) {
console.error('trouble parsing json!', e, text);
}
return text;
};

var TextParser = function(type,text,options) {
if (type === 'application/json') {
text = parseJson(text);
}

	this.tree = [{
		type: "codeblock",
		attributes: {
			code: {type: "string", value: text},
			language: {type: "string", value: type}
		}
	}];
};

exports["text/plain"] = TextParser;
exports["text/x-tiddlywiki"] = TextParser;
exports["application/javascript"] = TextParser;
exports["application/json"] = TextParser;
exports["text/css"] = TextParser;
exports["application/x-tiddler-dictionary"] = TextParser;

exports["application/hjson"] = TextParser;

})();

