/*\
jedit: :folding=explicit: {{{
created: 20190627230754834
title: $:/asekai/datafile/macros/kaicon
tags: $:/asekai/datafile/macros
type: application/javascript
module-type: macro

fetch a kai icon or entity icon

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";

/* Information about this macro */
exports.name = "kaicon";
exports.params = [ {name: "tiddler"}, {name: "strain"}, {name: "form"}, {name: "icon"} ];
// }}}

/*
Run the macro
*/
exports.run = function(tiddler, strain, form, icon) {
	var ident, ident2;
	const wiki = $tw.wiki;
	
	if (tiddler !== undefined) {
		tiddler = tiddler.replace(/^k_/, '');

		if (/_/.test(tiddler) === true) {
			ident2 = tiddler.split('_');
			strain = ident2[0];
			form = ident2[1];
		}
		else {
			strain = tiddler;
			form = tiddler;
		}
	}
	
	
	if (form === undefined || form === '' || strain === form) {
		ident = strain;
		form = strain;
	}
	else {
		ident = strain + '_' + form;
	}
	
	// 
	if (typeof icon !== 'string' || icon === '') {
		tiddler = 'k_'  + strain + '_' + form;
		icon = wiki.getTextReference(tiddler + '!!icon', 'side');
	}

	// fetch actual icon
	var iconText = wiki.getTextReference('Sprites/' + ident + '/' + icon, 'NOT FOUND');
	
	if (iconText === 'NOT FOUND') {
		iconText = wiki.getTextReference('Sprites/entity/side', 'NOT FOUND');
	}
	
	iconText = iconText.replace(/(\<\?xml[A-Za-z0-9\s"=\.-]+\?\>\n?)|\n/g, '');
	
	//onsole.log('kaicon', tiddler);
	return iconText;
};

})();