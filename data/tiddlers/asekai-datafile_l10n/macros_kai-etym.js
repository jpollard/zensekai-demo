/*\
jedit: :folding=explicit:
title: $:/asekai/datafile/macros/kai-etym.js
tags: $:/asekai/datafile/macros
type: application/javascript
module-type: macro

fancy kai etymology with links

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";

var kaiLangString; // {{{
//const nodeFound = (process.release.name === 'node' || !$tw);
const nodeFound = true;

if (nodeFound) {
	kaiLangString = require('$:/asekai/datafile/macros/kai-lang-string').run;
}
else {
	kaiLangString = function(ident, field) {
		var test = 
		'Balto (legendary sled dog) -> Alto, circus ("circle", la) + alto ("high", it)'
		; return test;
	};
}
// }}}

/* Information about this macro */
exports.name = "kai-etym";
exports.params = [ {name: "ident"} ];

var languages = { // {{{
	'en': {
		name: 'English',
		source: 'English_language'
	},
	
	'de': {
		name: 'German',
		source: 'German_language'
	},
	'el': {
		name: 'Greek',
		source: 'Greek_language'
	},
	'es': {
		name: 'Spanish',
		source: 'Spanish_language'
	},
	'et': {
		name: 'Estonian',
		source: 'Estonian_language'
	},
	'fa': {
		name: 'Persian',
		source: 'Persian_language'
	},
	'fi': {
		name: 'Finnish',
		source: 'Finnish_language'
	},
	'fr': {
		name: 'French',
		source: 'French_language'
	},
	'hi': {
		name: 'Hindi',
		source: 'Hindi'
	},
	'hy': {
		name: 'Armenian',
		source: 'Armenian_language'
	},
	'is': {
		name: 'Icelandic',
		source: 'Icelandic_language'
	},
	'it': {
		name: 'Italian',
		source: 'Italian_language'
	},
	'ja': {
		name: 'Japanese',
		source: 'Japanese_language'
	},
	'ka': {
		name: 'Georgian',
		source: 'Georgian_language'
	},
	'ko': {
		name: 'Korean',
		source: 'Korean_language'
	},
	'la': {
		name: 'Latin',
		source: 'Latin'
	},
	'mn': {
		name: 'Mongolian',
		source: 'Mongolian_language'
	},
	'nl': {
		name: 'Dutch',
		source: 'Dutch_language'
	},
	'ru': {
		name: 'Russian',
		source: 'Russian_language'
	},
	'sv': {
		name: 'Swedish',
		source: 'Swedish_language'
	},
	'zh': {
		name: 'Mandarin Chinese',
		source: 'Mandarin_Chinese'
	},
	
	
	'akk': {
		name: 'Akkadian',
		source: 'Akkadian_language'
	},
	'vol': {
		name: 'Volapük',
		source: 'Volapük'
	},
	'zho': {
		// for chinese dialects/minority languages
		// specify which in plain text
		name: 'Chinese',
		source: 'Varieties_of_Chinese'
	},
	
	
	'*PGMC': {
		name: 'Proto-Germanic, reconstructed',
		source: 'Proto-Germanic_language'
	},
	'*PIE': {
		name: 'Proto-Indo-European, reconstructed',
		source: 'Proto-Indo-European_language'
	}
	
	
}; // }}}

exports.link = function(code) { // {{{
	var lang;
	const prefix = 'https://en.wikipedia.org/wiki/';
	// LATER: give a way to change prefix -
	// in case for instance you want another language wiktionary
	
	lang = languages[code];
	//onsole.log('LINK', code);
	
	if (lang == undefined) {
		return code;
	}
	
	return '$1<a href="' + prefix + lang.source + '"><abbr title="' + lang.name + '">' + code + '</abbr></a>)';
}; // }}}


/*
Run the macro
*/
exports.run = function(ident) {
	var etym, etymReplaced, result = 'foo',
	    parenExp = /(\(|\,\s)([a-z]{2,3}|\*[A-Z]+)\)/,
	    parenExp2 = /.*?(\(|\,\s)([a-z]{2,3}|\*[A-Z]+)\).*/,
	    tempRegex, pushing, replaces = [], i;
	    
	// run lang string macro
	etym = kaiLangString(ident, 'name-etym');
	
	if (etym === undefined) {
		etym = '(not yet added)'; // LATER: localise
		return etym;
	}
	
	result = etymReplaced = etym;
	i = 0;
	
	// BUG: regex always returns the same code (the last one) twice.
	
	while (parenExp2.test(etymReplaced) === true && i < 20) {
		pushing = etymReplaced.replace(parenExp2, '$2');
		//onsole.log('etymreplaced', pushing, etymReplaced);
		replaces.push(pushing);
		etymReplaced = etymReplaced.replace(parenExp, '_');
		i++;
	}
	
	//onsole.log('REPLACES', replaces);
	var codeRegex;
	
	for (i = 0; i < replaces.length; i++) {
		codeRegex = new RegExp('(\\(|\\,\\s)' + replaces[i].replace('*', '\\*') + '\\)');
		//onsole.log('codeRegex', codeRegex);
		result = result.replace( codeRegex, exports.link(replaces[i]) );
	}
	
	return result;
};

if (nodeFound) {
	//onsole.log(exports.run());
}

// [all[shadows]has[name-etym]removeprefix[$:/asekai/lang/en/strains/]addprefix[k_]]

})();
