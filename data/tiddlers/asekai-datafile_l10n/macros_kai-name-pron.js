/*\
jedit: :folding=explicit: {{{
created: 20190628061749896
tags: $:/asekai/datafile/macros
title: $:/asekai/datafile/macros/kai-name-pron
type: application/javascript
module-type: macro

kai name etymology

\*/
(function(){
/*jslint node: true, browser: true, laxcomma: true */
/*global $tw: false */
"use strict";

var util = require('$:/asekai/datafile/util.js').util;
var heart = require('$:/asekai/datafile/lib/heart.js');
var kaiLangString = require('$:/asekai/datafile/macros/kai-lang-string').run;

/* Information about this macro */
exports.name = "kai-name-pron";
exports.params = [ {name: "ident"}, {name: "mode"} ];
// }}}


/* helper funcs to be shared by pron list & pron key */

// clean up unpronounced symbols, such as (h)json characters
const filterPronField = function(field) {
	field.replace(/(\"?[o]\"?\:\s?\".+\")|\"?[p]\"?\:|^\[|\]$/g, '');
	field = field.replace(/^(.+)\"?n\"?\:\s?(\[(\".+\"\,?)+\])(.+)$/g, '$1$3$2');
	field = field.replace(/[\{\}\'\"\,]/g, ' ');
	return field;
};

const flattenPron = function(pron, variantPron) { // {{{
	var paren, narrow, result;
	
	result = '/' + variantPron + pron.p + '/';
	
	// 'narrow' (bracket) pronunciations
	if (pron.n !== undefined) {
		narrow = pron.n;
	}
	
	// 'original' script
	if (pron.o !== undefined) {
		paren = pron.o;
	}
	
	
	if (narrow !== undefined) {
		result += ( ' - [' + narrow.join('], [') + ']' );
	}
	
	if (paren !== undefined) {
		result += (' (' + paren + ')');
	}
	
	return result;
}; // }}}

// print the text of each pronunciation in the list
const pronList = function(field, variantPron) { // {{{
	var parsed, list = '', listItem, i;
		
	parsed = heart.parseNamePron(field);
	
	for (i = 0; i < parsed.length; i++) {
		listItem = '';
		
		if (i > 0) {
			variantPron = '';
		}
		
		// deal with empty pronunciations
		if ('"' + parsed[i] == '"') {
			// LATER: make a localisation string
			listItem = '(not yet added)';
		}
		else if (typeof parsed[i] == 'object') {
			listItem = flattenPron( parsed[i], variantPron );
		}
		else {
			listItem = ('/' + variantPron + parsed[i] + '/');
		}
		
		list += ('<li>' + listItem + '</li>');
		
		// if pronunciations empty, stop
		if (i == 0 && ('"' + parsed[i]) == '"') {
			break;
		}
	}
	
	list = '<ol>\n\t\t' + list + '\n\t</ol>';
	//onsole.log('PRONLIST', list);
	
	return list;
}; // }}}

const pronKey = function(symbols) { // {{{
	var result = '', symDesc, symName, i;
	
	// sort symbols by appearance in string
	symbols.sort(function(a, b) {
		return a.index - b.index;
	});
		
	// print pronunciation for each symbol
	for (i = 0; i < symbols.length; i++) {
		//onsole.log('symbols', i, [symbols[i]]);
		
		symDesc = symbols[i][1];
		symName = symbols[i][0];
		
		result += ('\n    <li>' + symName + ' - ' + ( symDesc.replace(/_([^_]+)_/g, '<b>$1</b>') ) + '</li>');
	}
	
	result = '<div class="pronkey"><ul>' + result + '\n\t</ul></div>';
	return result;
};
// }}}


exports.pronObj = function(ident, mode) {
	var sym, symTid, symOrder = [], i;
	
	symTid = $tw.wiki.getTextReference('$:/asekai/lang/en/pronkey!!text', '');
	sym = heart.hjsonParse(symTid);
	//onsole.log('symtid', symTid);

	try {
		var tid, field, variantPron = '', fieldLeft, fieldLeftCache;
		
		tid = kaiLangString(ident, 'tiddler');
		field = tid['name-pron'];
		
		variantPron = kaiLangString(ident, 'variant-pron');
		
		// cleanup pronunciation field
		if (field === undefined) {
			field = '';
		}
		fieldLeft = filterPronField(field);
		fieldLeftCache = fieldLeft;
		//onsole.log('FIELDLEFT', fieldLeftCache);
		
		//onsole.log('PRONUNCIATION', ident, fieldLeft);
		
		// build list of pronunciations
		var regex, regexExec, symbols = [];
		
		for (i in sym) {
			if (/\\u|[\(\)\+]/.test(i) === true) {
				regex = new RegExp( i, 'g' );
			}
			else {
				regex = new RegExp( heart.escapeUnicode(i), 'g' );
			}
			
			//onsole.log('regex', i, regex); 
			
			// if symbol found, add to list
			if (regex.test(fieldLeft) === true) {
				// fix simple entries
				if (typeof sym[i] === 'string') {
					sym[i] = [ i, sym[i] ];
				}
				
				// remove the symbol unless offered a replacement expression
				if (sym[i][2] !== undefined) {
					fieldLeft = fieldLeft.replace(regex, sym[i][2]);
				}
				else {
					fieldLeft = fieldLeft.replace(regex, '');
				}
				
				// record index of match & add symbol to list
				regexExec = regex.exec(fieldLeftCache);
				
				if (regexExec !== null) {
					sym[i].index = regexExec.index;
				}
				
				symbols.push(sym[i]);
			}
			
			//onsole.log('fieldleft', field, fieldLeft, i, regexExec);
			
			// if string has been emptied, finish
			if (fieldLeft === '') {
				break;
			}
		}
		
		
		//onsole.log('SYMBOLS', symbols);
		
		var result = {
			list: '',
			key: ''
		};
		
		// output pronunciation
		if (mode !== 'key') {
			result.list = pronList(field, variantPron);
		}
		if (mode !== 'list') {
			result.key = pronKey(symbols);
		}
	}
	catch (e) {
		result = 'error';
		console.error('kai-name-pron error:', e.message, tid);
	}
	
	return result;
};
	

/*
Run the macro
*/
exports.run = function(ident, mode) {
	var pronObj = exports.pronObj(ident, mode);
	
	// output pronunciation
	if (mode == 'list') {
		return '' + pronObj.list;
	}
	else if (mode == 'key') {
		return '' + pronObj.key;
	}
	
	return '' + pronObj.list + '\n' + pronObj.key;
};

})();