/*\
jedit: :folding=explicit:wrap=soft:
created: 20190324081121909
tags: $:/plugins/takumi/asekai-saver
title: $:/plugins/takumi/asekai-saver/saver.js
type: application/javascript
module-type: saver

return a sector-specific kai index

\*/
(function(){// {{{
/*jslint node: true, browser: true */
/*global $tw: false, netscape: false, Components: false */
"use strict";

var jsonutil = require('$:/asekai/datafile/saver/json.js').json;


var AsekaiSaver = function(wiki) {
	this.wiki = wiki;
};

AsekaiSaver.TYPES = { //
	'tw': {
		'ext': '.html', 'type': 'text/html'
	}
	, 'min': {
		'ext': '.min.html', 'type': 'application/x-tiddler-html-div'
	}
	, 'plugin': {
		'ext': '_plugin.json', 'type': 'application/json',
		'asekai-datatype': 'plugin'
	}
	, 'json': {
		'ext': '.json', 'type': 'application/json',
		'asekai-datatype': 'jsondata'
	}
	, 'jsongz': {
		'ext': '.json.gz', 'type': 'application/gzip',
		'asekai-datatype': 'jsongz'
	}
	, 'msgpack': {
		'ext': '.msgpack.bin', 'type': 'application/x.msgpack'
	}
}; //

AsekaiSaver.SETS = {
	plugin: [ 'plugin' ]
	
	// 'plugin', 'jsongz'
	// 'min', 'plugin', 'json', 'jsongz'
	// 'plugin', 'json', 'json.gz'
};

AsekaiSaver.SAVE = AsekaiSaver.SETS.plugin;

// }}}

AsekaiSaver.prototype.saveBlock = function(path, filename, filetype, text, callback) { // {{{
	var messageBox, message, event, i;
	messageBox = document.getElementById("tiddlyfox-message-box");
	
	// filename filter:
	// $:/state/asekai-saver/text +[removeprefix[$:/state/asekai-saver/text]addprefix[../../]]
	
	// filenames[ savingTo[i] ] = filename.replace('.html', AsekaiSaver.TYPES[ savingTo[i] ].ext);
	
	if ($tw.syncadaptor) {
		var d = new Date(),
		dateversion = '' + (d.getFullYear() - 2000) + '.' + (d.getMonth() + 1) + '.' + d.getDate();
		
		var typesInfo = {
			"text": text
			, "author": "Takumi"
			, "core-version": ">=5.0.0"
			, "description": "Asekai datafile testing plugin"
			, "list": "readme"
			, "plugin-type": "plugin"
			, "hide-body": "yes"
			, "version": dateversion
			, "tags": "data_output SecretDevStorylist"
			
		};
		
		for (i in AsekaiSaver.TYPES[ filetype ]) {
			typesInfo[i] = AsekaiSaver.TYPES[ filetype ][i];
		}
		
		typesInfo.title = '$:/asekai/datafile/saver/text/' + filename + (typesInfo['ext'].replace(/\./g, '_') );
		delete typesInfo.ext;
		
		// a file can easily be saved outside the wiki folder with the regular
		// addTiddler command
		// and this will not be loaded when the wiki loads.
		
		$tw.wiki.deleteTiddler( typesInfo.title );
		$tw.wiki.addTiddler( new $tw.Tiddler(typesInfo) );
	}
	
	
	// IF USING TIDDLYFOX:

	/*
	// Create the message element and put it in the message box
	message = document.createElement("div");
	message.setAttribute("data-tiddlyfox-path",decodeURIComponent(pathname));
	message.setAttribute("data-tiddlyfox-content",text);
	
	//onsole.log('messagebox allfiles', messageBox.dataset.savedfiles, messageBox.dataset.allfiles);

	messageBox.appendChild(message);

	// Add an event handler for when the file has been saved
	message.addEventListener("tiddlyfox-have-saved-file",function(event) {
		var savedfiles, allfiles;
		
		messageBox = document.getElementById("tiddlyfox-message-box");
		
		savedfiles = (messageBox.dataset.savedfiles * 1) + 1;
		allfiles = (messageBox.dataset.allfiles * 1);
		
		messageBox.dataset.savedfiles = savedfiles;
		
		if (savedfiles === allfiles) {
			//onsole.log('ALL FILES SAVED');
			
			// I don't know exactly what the callback does
			// it appears to show the message
			// but I'm not sure
			callback(null);
		}
		
		
	}, false);

	// Create and dispatch the custom event to the extension
	event = document.createEvent("Events");
	event.initEvent("tiddlyfox-save-file",true,false);
	message.dispatchEvent(event);
	*/
	
	//onsole.log('ASEKAI SAVER', text);
}; // }}}

AsekaiSaver.prototype.save = function(text,method,callback,options) {
	// method will be 'save'/'autosave'
	var pathname = '', filename, messageBox, i,
	    generatorName;
	
	var filenames = {},
	dataSmall = {
		'min': null, 'plugin': null, 'json': null,
		'jsongz': null, 'msgpack': null
	},
	coreSmall, savingTo;
	const STRIP_TW = true, NO_STRIP_TW = false, RETURN_OBJ = true, NO_RETURN_OBJ = false;
	
	// which formats saving to
	savingTo = AsekaiSaver.SAVE;
	
	

	messageBox = document.getElementById("tiddlyfox-message-box");
	
	// tiddlyfox has to deal with real filenames
	// this code is just taken from the regular tiddlyfox addon.
	if (messageBox) { // {{{
		// Get the pathname of this document
		pathname = document.location.toString().split("#")[0];
		// Replace file://localhost/ with file:///
		if(pathname.indexOf("file://localhost/") === 0) {
			pathname = "file://" + pathname.substr(16);
		}
		// Windows path file:///x:/blah/blah --> x:\blah\blah
		if(/^file\:\/\/\/[A-Z]\:\//i.test(pathname)) {
			// Remove the leading slash and convert slashes to backslashes
			pathname = pathname.substr(8).replace(/\//g,"\\");
		// Firefox Windows network path file://///server/share/blah/blah --> //server/share/blah/blah
		} else if(pathname.indexOf("file://///") === 0) {
			pathname = "\\\\" + unescape(pathname.substr(10)).replace(/\//g,"\\");
		// Mac/Unix local path file:///path/path --> /path/path
		} else if(pathname.indexOf("file:///") === 0) {
			pathname = unescape(pathname.substr(7));
		// Mac/Unix local path file:/path/path --> /path/path
		} else if(pathname.indexOf("file:/") === 0) {
			pathname = unescape(pathname.substr(5));
		// Otherwise Windows networth path file://server/share/path/path --> \\server\share\path\path
		} else {
			pathname = "\\\\" + unescape(pathname.substr(7)).replace(new RegExp("/","g"),"\\");
		}
		
		generatorName = 'asekai-saver/saver.js [on tiddlyfox]';
	} // }}}
	// TODO: $tw.syncadaptor url
	else if ($tw.syncadaptor) {
		filename = 'Asekai_crunch';
		generatorName = 'asekai-saver/saver.js [on Node]';
	}
	
	if(messageBox || $tw.syncadaptor) {
		if (messageBox) {
			messageBox.dataset.allfiles = savingTo.length + '';
			messageBox.dataset.savedfiles = '0';
			
			// save normal big file
			this.saveBlock(pathname, filename, 'tw', text, callback);
		}
		
		// don't autosave extra files
		if (method == 'autosave') {
			return true;
		}
		
		
		// if array is zero length we're done,
		// otherwise save files
		if (savingTo.length > 0) {
			var coreJson;
			
			// save storeArea-only file
			if (savingTo.indexOf('min') !== -1) {
				coreSmall = jsonutil.coreify(text);
				coreSmall = jsonutil.unpackPlugins(coreSmall);
				coreSmall = jsonutil.peel(coreSmall);
				
				dataSmall.min = coreSmall.toString();
				this.saveBlock(pathname, filename, 'min', dataSmall.min, callback);
			}
			
			// save json tw plugin
			if (savingTo.indexOf('plugin') !== -1) {
				coreJson = jsonutil.makeJson(coreSmall, NO_STRIP_TW, RETURN_OBJ, generatorName);
				
				dataSmall.plugin = JSON.stringify(coreJson, null, 2);
				this.saveBlock(pathname, filename, 'plugin', dataSmall.plugin, callback);
			}
			
			// save minimised & parsed json
			if (savingTo.indexOf('json') !== -1 ||
				savingTo.indexOf('jsongz') !== -1 || savingTo.indexOf('msgpack') !== -1)
			{
				coreJson = jsonutil.makeJson(coreJson, STRIP_TW, RETURN_OBJ, generatorName);
				
				if (savingTo.indexOf('json') !== -1) {
					dataSmall.json = JSON.stringify(coreJson);
					this.saveBlock(pathname, filename, 'json', dataSmall.json, callback);
				}
			}
			
			// save gzipped json
			if (savingTo.indexOf('jsongz') !== -1) {
				dataSmall.jsongz = jsonutil.makeJsonGz(coreJson);
				this.saveBlock(pathname, filename, 'jsongz', dataSmall.jsongz, callback);
			}
			
			// save msgpack
			if (savingTo.indexOf('msgpack') !== -1) {
				dataSmall.msgpack = jsonutil.makeMsgpack(coreJson);
				this.saveBlock(pathname, filename, 'msgpack', dataSmall.msgpack, callback);
			}
			
			//onsole.info('SAVING FINISHED', dataSmall);
			return true;
		}
	}
	
	return false;
	// if tiddlyfox won't save tw just uses another saver anyway
};

/*
Information and creator function
*/
AsekaiSaver.prototype.info = {
	name: "asekai",
	priority: 1,
	capabilities: ["save"]
};

exports.canSave = function(wiki) {                 
	// TODO: can save if tiddlyfox or $tw.syncadaptor
	
	return true;
};

exports.create = function(wiki) {
	return new AsekaiSaver(wiki);
};

})();
