/*\
jedit: :folding=explicit:wrap=soft:
created: 20190324081121909
tags: $:/plugins/takumi/asekai-saver
title: $:/asekai/datafile/saver/json.js
type: application/javascript
module-type: library

saver json

\*/
(function(){
// requires etc {{{
/*jslint node: true, browser: true, esnext: true, laxcomma: true */
/*global $tw: false */
"use strict";

var
msgpack = require('$:/plugins/creationix/msgpack/msgpack.js')
, pako = require('$:/plugins/nodeca/pako/pako.min.js')
, heart = require('$:/asekai/datafile/lib/heart.js')
, KaiStrainPlugin = require('$:/asekai/datafile/startup/kai-strain.js');

// clean this up later
var globals = { // {{{
	sectors: {
		"1": "nature",
		"2": "culture",
		"3": "spirit",
		"4": "physics",
		
		"1a": "freeze",
		"2a": "burn",
		"3a": "stain",
		"4a": "purge"
	},
	sectorNames: {
		"1": "Nature",
		"2": "Culture",
		"3": "Spirit",
		"4": "Physics",
		
		"1a": "Freeze",
		"2a": "Burn",
		"3a": "Stain",
		"4a": "Purge"
	}
}; // }}}
// }}}


var jsonmod = {
name: "saver-json"


// remove later? {{{

// unpack plugins
, unpackPlugins: function(core) { // {{{
	var result, plugins, plugin, disabled, tid, inheritTid, unpackedTids = {},
	    i, j, k;
	
	//const peelListDefault = [ '[title="$:/core"]', '[title^="$:/themes/"]', '[title^="$:/temp/"]', '[title^="$:/state/"]' ];
	
	//plugins = core.querySelectorAll('[plugin-type="plugin"]');
	plugins = core.querySelectorAll('[title^="$:/asekai/modules/"], [title^="$:/asekai/language/"]');

	//$:/config/Plugins/Disabled/$:/asekai/modules/strains/feuvog
	
	for (i = 0; i < plugins.length; i++) {

		// don't unpack plugins that are disabled
		disabled = core.querySelector('[title="$:/config/Plugins/Disabled/' + plugins[i].getAttribute('title') + '"]');
		if (disabled !== null && heart.findPre(disabled) === 'yes') {
			continue;
		}
		
		
		if (plugins[i].getAttribute('type') == 'application/json' && plugins[i].getAttribute('plugin-type') != 'kai-strain') {
			try {
			plugin = heart.findPre(plugins[i]);
			plugin = JSON.parse(plugin).tiddlers;
			//onsole.log('unpack plugins test', plugin);
	
			for (j in plugin) {
				
				// do not unpack tid if it's empty or already exists
				// unless it is a kai/localisation tid, in which case text is usually empty so unpack
				if (  (/^(k_|\$:\/asekai\/lang\/)/.test(j) === true && core.querySelector('[title="' + j + '"]') !== null) ||
					(/^(k_|\$:\/asekai\/lang\/)/.test(j) === false && (plugin[j].text === undefined || plugin[j].text == ''))  ) {
					continue;
				}
	
				unpackedTids[j] = plugin[j];
			}
			
			// remove duplication in plugin tid
			core.removeChild(plugins[i]);
			}
			catch (e) {
				console.error('issue unpacking plugin', e);
			}
		}
		else if (plugins[i].getAttribute('plugin-type') == 'kai-strain' || plugins[i].getAttribute('plugin-type') == 'kai-lang') {
			try {
				// these are already unpacked,
				// so simply try to get the plugin tiddlers
				// e.g.: $:/asekai/modules/strains/hokadou +[plugintiddlers[]]
				
				var pluginTiddlers = $tw.wiki.filterTiddlers( plugins[i].getAttribute('title') + ' +[plugintiddlers[]]' );
				
				//onsole.log('kai strain plugintiddlers', pluginTiddlers);
				
				for (j = 0; j < pluginTiddlers.length; j++) {
					tid = $tw.wiki.getTiddler(pluginTiddlers[j]);
					unpackedTids[tid.fields.title] = heart.unfreeze(tid.fields);
					
					delete unpackedTids[tid.fields.title].created;
					delete unpackedTids[tid.fields.title].modified;
				}
				
			}
			catch (e) {
				console.error('issue unpacking kai plugin', e);
			}
		}
	}
	
	
	for (j in unpackedTids) {
		// copy info from 'inherit' tids
		if ( unpackedTids[j].inherit !== undefined ) {
			if (/^\$:\/asekai\/lang/.test(j) === true) {
				inheritTid = j.replace(/(\$:\/asekai\/lang\/[a-z]+\/[a-z-]+\/)[a-z_-]+/, '$1') + unpackedTids[j].inherit;
			}
			else if (/^k_/.test(j) === true) {
				inheritTid = 'k_' + unpackedTids[j].inherit;
			}
			else {
				inheritTid = unpackedTids[j].inherit;
			}
			
			//unpackedTids[j]['DEBUG-inherit'] = inheritTid;
			inheritTid = unpackedTids[inheritTid];
			
			if (inheritTid === undefined) {
				inheritTid = core.querySelector('[title="' + inheritTid + '"]');
				
				if (inheritTid !== null) {
					inheritTid = this.flattenAttributes(inheritTid);
				}
			}
			
			//unpackedTids[j]['DEBUG-inherittid'] = (JSON.stringify(inheritTid));
			
			if (inheritTid !== null) {
				for (k in inheritTid) { //
					if (unpackedTids[j][k] === undefined) {
						unpackedTids[j][k] = inheritTid[k];
					}
				} //
			}
		}
		
		tid = heart.createTid(j, unpackedTids[j].text, unpackedTids[j]);
		core.appendChild(tid);
	}
	
	
	result = core;
	return result;
} // }}}

// strip down tw page to just the storeArea
// 116,857 / 2,022,629 = 6%. very big file reduction
, peel: function(core) { // {{{
	var result, i, j, elements, peelList;
	const peelListDefault = [ '[title^="$:/"]' ];
	
	peelList = $tw.wiki.getTextReference('$:/plugins/takumi/asekai-saver/settings/strip');
	
	peelList = JSON.parse(peelList)['0'];
	//onsole.log('peellist', peelList);
	
	for (i = 0; i < peelList.length; i++) {
		elements = core.querySelectorAll(peelList[i]);
		
		//onsole.log('query selector', i, elements[i], elements);
		
		for (j = 0; j < elements.length; j++) {
			if ( /^\$:\/asekai\/(lang|config)/.test(elements[j].getAttribute('title')) ) {
				continue;
			}
			
			elements[j].parentNode.removeChild(elements[j]);
		}
	}

	result = core;
	return result;
} // }}}



// remove 'core' (storearea) from tw data
// and equip it with a string to clean it up
, coreify: function(text) {
	var dom, core;
	// avoid confusion with two kinds of "this"
	const coreString = this.coreString;
	
	dom = new DOMParser().parseFromString(text, "text/html");
	core = dom.querySelector('#storeArea');
	
	core.toString = function() {
		return coreString(this);
	};
	
	return core;
}

, coreString: function(coreNode) {
	var result;
	const nastyRegex = new RegExp("(>(\n|\\n){1,2})(\n|\\n)+(<" + 'div)', 'g');
	
	result = coreNode.outerHTML;
	
	// clean leftover newlines
	// the div tag in this regex seems to crash the entire tw even as a comment,
	// so I split up that nasty div tag string
	result = result.replace(nastyRegex, '$1$4');
	
	return result;
}


// }}}



// TODO: allow heart to accept json

// get tiddlers simply using tiddlywiki apis
, jsonTidsData: function(filter, stripTags, parseText) {
	var plugin = {}, tids, wiki, i, j;
	
	wiki = $tw.wiki;
	tids = wiki.filterTiddlers(filter);
	
	for (i = 0; i < tids.length; i++) {
		plugin[ tids[i] ] = wiki.getTiddler( tids[i] );
		plugin[tids[i]] = heart.flattenAttributes( plugin[tids[i]] );
		
		// parse string values
		if (parseText === true) {
			if ( plugin[tids[i]]['type'] == 'application/json' ) {
				try {
					plugin[tids[i]].text = JSON.parse( plugin[tids[i]].text );
				}
				catch (e) {
					console.log("json parse of", tids[i], "failed");
				}
			}
		}
		
		for (j = 0; j < stripTags.length; j++) {
			delete plugin[tids[i]][ stripTags[j] ];
		}
	}
	
	return plugin;
}

// make object or json string
, makeJson: function(core, stripTW, returnObj, generatorName) {
	var filter, stripTags, plugin, result;
	
	const filterBase =
	"[all[tiddlers+shadows]prefix[$:/asekai/lang/]] " +
	"[all[tiddlers+shadows]!is[system]" +
	"!prefix[Draft of ']!suffix[~]!prefix[/home/]!prefix[/media/]" +
	"!tag[prettying]!tag[remove_in_min]sort[title]";
	const filterBase2 = "] -GettingStarted";
	
	
	if (generatorName === undefined) {
		generatorName = "asekai-saver/json.js";
	}
	
	if (stripTW === true) {
		filter = filterBase + "!tag[only_in_guide]" + filterBase2;
		stripTags = [
			'created','modified','title','revision','bag',  'tags'
		];
	}
	else {
		filter = filterBase + "" + filterBase2;
		stripTags = [
			'created','modified','title','revision','bag'
		];
	}
	
	
	//plugin = heart.makeJSON(core, stripTW);
	plugin = this.jsonTidsData(filter, stripTags, stripTW);
	
	// add readme if not minimal
	if (stripTW !== true) {
		plugin[ '$:/asekai/datafile/data/readme' ] = {
			text:
			"This plugin contains a cached copy of the data from the asekai datafile, for testing the tw kai guide template.\n\n" +
			"It was generated by " + generatorName + " on " + (heart.dateString())
		};
		
		plugin = { tiddlers: plugin };
	}
	
	if (returnObj !== true) {
		result = JSON.stringify(plugin);
	}
	else {
		result = plugin;
	}
	
	return result;
}


, makeJsonGz: function(json) {
	var data, packed, string;
	
	data = JSON.stringify(json);
	packed = pako.deflate(data);
	string = this.bufferToHex(packed);
	return string;
}

, makeMsgpack: function(json) {
	var plugin, packed, string;
	
	packed = msgpack.encode(json);
	string = this.bufferToHex(packed);
	
	return string;
}


// convert binary data (msgpack etc) to sanitised string
// this is a workaround to pass it to tiddlyfox
// which must pick up data as a string from an html data- attribute,
// but I managed to modify the addon to write escaped bytes as binary.
, bufferToHex: function(buffer) {
// https://stackoverflow.com/questions/40031688
	return Array
		.from (new Uint8Array (buffer))
		.map (b => '\\x' + (b.toString (16).padStart (2, "0")) )
		.join ("");
}






};

exports.json = jsonmod;

})();