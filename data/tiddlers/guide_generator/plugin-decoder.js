/*\
jedit: :folding=explicit: {{{
created: 20191224092502511
title: $:/asekai/datafile/lib/plugin-decoder.js
tags: $:/asekai/datafile
type: application/javascript
module-type: library

helper functions for decoding hjson structures such as kai strain plugins

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";
// }}}

exports.PluginDecoder = {
	// check if field in kai plugin is empty or not
	fieldIsEmpty: function(field) {
		if (field === undefined || field == '' ||
		(typeof field == 'object' && field.constructor.name == 'Array' && field.length < 1)
		) {
			return true;
		}
		
		return false;
	}
};

})();