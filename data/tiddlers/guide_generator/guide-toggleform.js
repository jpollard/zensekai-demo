/*\
jedit: :folding=explicit: {{{
created: 20200202092706357
title: $:/asekai/datafile/macros/guide-toggleform.js
tags: 
type: application/javascript
module-type: macro

javascript recode of kaiguide generator

\*/
(function(){
/*jslint node: true, browser: true, esnext: true */
/*global $tw: false */
"use strict";

exports.name = "guide-toggleform";
exports.params = [ {name: "strainfilter"}, {name: "formfilter"} ];
// }}}


exports.toggleList = function(entryFilter, toggleName) { // {{{
	var entryList, noneItem, item, itemsList, i;
	const wiki = $tw.wiki, toggleItem = exports.toggleItem;
	
	entryList = wiki.filterTiddlers(entryFilter);
	
	noneItem = toggleItem('none', toggleName, 'checked');
	itemsList = [ '<!-- \\n -->', noneItem ];
	
	for (i = 0; i < entryList.length; i++) {
		item = toggleItem(entryList[i], toggleName);
		itemsList.push(item);
	}
	
	return itemsList;
}; // }}}

exports.toggleItem = function(pageid, inputName, isChecked) { // {{{
	var result, checked = '';
	
	if (typeof isChecked == 'string' || isChecked === true) {
		checked = 'checked ';
	}
	
	result = '<input id="entry-'+ pageid +'" data-page="'+ pageid +'" '+
	'name="'+ inputName +'-toggle" type="radio" class="page-selector '+ inputName +'-toggle" '+
	'title="'+ pageid +'" tabindex="0" aria-hidden="false" '+ checked +'/>';
	
	// $inputname$-toggle-$pageid$ tabindex="-1" aria-hidden="true"
	
	return result;
}; // }}}


exports.run = function(strainFilter, formFilter) { 
	var result, strainList, formList;
	const wiki = $tw.wiki;
	
	strainList = exports.toggleList(strainFilter, 'strain');
	formList = exports.toggleList(formFilter, 'form');
	
	result = strainList.concat(formList).join('\n');
	// although guide-sing works with arrays render_guide.sh does not
	
	return result;
};
})();