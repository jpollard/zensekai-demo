/*\
jedit: :folding=explicit: {{{
created: 20200215041728691
title: $:/asekai/datafile/macros/guide-spectrom.js
tags: 
type: application/javascript
module-type: macro

spectrom

\*/
(function(){
/*jslint node: true, esnext: true */
/*global $tw: false */
"use strict";

exports.name = "guide-spectrom";
exports.params = [ {name: "ident"} ];
// }}}

// import child process
const child_process = require('child_process');

// serialise a tid to a "simpler" name : value format
exports.formatTid = function(tid) {
	const exclude = [
	'title', 'asekai-datatype', 'created', 'tags',
	'num-basic', 'num-strain', 'num-ident', 'number',
	'sorting-sct', 'sorting-idx', 'sorting-stg', 'sorting-rty'
	];
	const types = {
	};
	var fields = [], j, propName, propValue;
	
	// title is excluded so it can be treated specially
	fields.push('#TITLE : ' + tid.fields.title + ' : ' + tid.fields['asekai-datatype']);
	
	for (j in tid.fields) {
		if (exclude.indexOf(j) == -1) {
			propName = j.replace('-', '_');
			propValue = tid.fields[j];
			
			if (types[propName] === 'u8') {
				propValue = propValue * 1;
			}
			
			fields.push(propName + ' : ' + propValue);
		}
	};
	
	fields = fields.join("\n");
	return fields;
};

// this macro exports the data in a 'complete' tiddlywiki,
// i.e. after plugins have been unpacked & everything
// "LATER", may make an asekai plugin unpacker for rust / command line
// so tiddlywiki doesnt even have to boot.
exports.run = function() {
	var filter, tid, tids = [], fields, propName, i, j, result;
	const wiki = $tw.wiki;
	
	filter = wiki.filterTiddlers('k_punk k_coffee');
	
	// tids are in the order the filter sorted them
	for (i = 0; i < filter.length; i++) {
		tid = wiki.getTiddler(filter[i]);
		fields = exports.formatTid(tid);
		tids.push(fields);
	}
	
	var json;
	json = tids.join("\n\n");
	console.log('JSON', json);
	
	result = json;
	
	// you technically "aren't supposed to modify files" in a macro
	// but this isnt serving pages for a browser so i do what i want
	/*child_process.execSync('./spectrom/test "' + json + '"', (err, stdout, stderr) => {
		if (err) {
			console.error('child_process: could not execute command');
			return;
		}
	
		// the *entire* stdout and stderr (buffered)
		//console.log(`stdout: ${stdout}`);
		
		return stdout;
	});*/
	
	return result;
};

})();