/*\
jedit: :folding=explicit: {{{
created: 20191203074850830
tags: [[00 Internal]]
title: guide_generator/comment
type: application/javascript
module-type: widget

generate comment; copied from text widget.

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;
var fakeDocument = require("$:/core/modules/utils/fakedom.js").fakeDocument;

var CommentWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
	this.document = fakeDocument;
};

/*
Inherit from the base widget class
*/
CommentWidget.prototype = new Widget();

/*
Compute the internal state of the widget
*/
CommentWidget.prototype.execute = function() {
	var text;
	
	this.namespace = "http://www.w3.org/1999/xhtml";
	this.setVariable("namespace",this.namespace);
	
	this.makeChildWidgets();
};
// }}}

/*
Render this widget into the DOM
*/
CommentWidget.prototype.render = function(parent,nextSibling) {
	this.parentDomNode = parent;
	this.computeAttributes();
	this.execute();
	
	var text = this.getAttribute("text",this.parseTreeNode.text || "");
	text = text.replace(/[\r\<\>]/mg,"");
	
	// tiddlywiki's element creation is sandboxed and doesn't have createComment
	// so it must be added to fakeDom. and thus i did
	var commentNode = this.document.createComment(text);
	
	parent.insertBefore(commentNode, nextSibling);
	this.domNodes.push(commentNode);
	
	//onsole.log('PARSE TREE NODE', this.parseTreeNode);
	//onsole.log('COMMENT DEBUG', commentNode);
	//onsole.log('PARENTNODE', this.parentDomNode.innerHTML);
};

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
CommentWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(changedAttributes.text) {
		this.refreshSelf();
		return true;
	} else {
		return false;	
	}
};

exports.comment = CommentWidget;

})();