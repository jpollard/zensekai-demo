author: Takumi
core-version: >=5.0.0
created: 20190322082910014
description: Asekai strain "Nightwalker"
list: readme
plugin-type: kai-strain
strain-index: $:/asekai/modules/strain-index
tags: kai-strain k_nightwalker
title: $:/asekai/modules/strains/nightwalker
type: application/hjson
version: 20.04.14

// :wrap=soft:
{

readme:
"asekai basic strains: nightwalker"


k_nightwalker: {
	number: { q: 2, s: 2 }
	
	tags: []
	
	l10n: { en: {
		phenoclass: "ghostly weasel"
	}}
}


sigils: "|!goodroute||\n|!badroute||"

icons: {
	nightwalker: side
}

meta: {
	youth: [
		{ to: "nightwalker",  type: "grow",  time: 1 }
		{ to: "nightcrawler", type: "grow",  time: 1,  kerv: 10 }
	]
	
	nightwalker: [
		{ to: "nightcrawler", type: "curve" }
	]
}



summary: { en: {
	default:
	"A sly and irreverent creature that lives life at maximum speed and exists to please no one. Its strong taste for adventure, risk, and novelty often gets it into trouble."
}}



"k/nightwalker": { "_DIR": "strain"

youth: {
	align: "6/5/nfep"
	num-sector: 2
	stage: b0
	
	l10n: { en: {
		name: { v: "Youth", n: "Nightwalker"
			pron: [ "" ]
			mean: ""
			etym: ''
		}
	}}
}


nightwalker: {
	align: "7/6/nfep"
	num-sector: 2
	variant: paneer
	stage: { b: "b", n: "bc" }
	
	l10n: { en: {
		name: { n: "Nightwalker"
			pron: [ { p: "naɪt.wɑkɚ", n: ["wɔk"] } ]
			mean: "night wanderer"
			etym: 'night walker (no special meaning)'
		}
	}}
}



nightcrawler: {
	align: "?/?/char"
	num-sector: "1a"
	stage: { b: "m1", m: true }
	
	l10n: { en: {
		name: { n: "Duskstepper"
			pron: [ "dʌsk.stɛpɚ" ]
			mean: ""
			etym: ''
		}
	}}
}

}



"p_basic/nightwalker": {
	"text": { "nightwalker": {
		"colours": {
		"z": "#aca040",
		"a": "#407482", "b": "#27464f",
		"c": "#6ff7fa", "d": "#3791c4",
		"e": "#4bfb49", "f": "#ff46ba",
		"g": "#e1e9ff",
		"h": "#8696fb", "i": "#f5f8ff"
		},
		"arrange": [
		"z", "a", "b", "c", "d", "e", "f", "g", "h", "i"
		],
		"map": {
		"background": "z", "seep": "d",
		"out2": "b", "fill2": "a",
		"out1": "d", "fill1": "c",
		"eyes1": "h", "eyemid1": "i",
		"orn1": "f", "orn2": "e",
		"teeth1": "g"
		}
		}
	}
}



"Sprites/nightwalker/side":
'<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n<svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" version="1.1"\n\theight="64px" width="64px" viewBox="0 0 32 32">\n<style>\npath {\n\tdisplay:inline;overflow:visible;visibility:visible;opacity:1;\n\tfill-opacity:1;fill-rule:nonzero;enable-background:accumulate;\n\tstroke:none;\n}\n</style>\n\n<rect class="background" x="0" y="0" width="32" height="32" style="fill:#aca040;"\n\t/>\n<path class="seep" style="fill:#3791c4;"\n\td="m 22,0 1,1 1,1 1,1 -1,1 v 2 2 2 l 1,1 1,1 v 2 l 1,1 -1,1 -1,1 -1,1 V 16 14 L 23,13 22,12 V 10 8 L 21,7 20,6 h -2 -2 l 1,1 1,1 h -2 l 1,1 1,1 H 16 V 8 6 L 15,7 14,8 13,9 12,8 H 10 8 L 7,9 6,10 H 4 2 0 v 2 2 l 1,1 1,1 -1,1 -1,1 h 2 l 1,-1 1,1 H 2 l -1,1 -1,1 v 2 2 2 l 1,1 1,1 1,1 1,1 h 2 2 2 2 2 l 1,1 1,1 h 2 2 l 1,-1 1,-1 v -2 l -1,1 -1,-1 v -2 l 1,1 1,1 1,1 1,1 h 2 2 l 1,-1 1,-1 1,-1 1,-1 v -2 -2 l -1,-1 -1,-1 -1,-1 1,-1 v -2 l 1,-1 -1,-1 1,-1 -1,-1 1,-1 -1,-1 1,-1 1,-1 H 30 L 29,9 28,8 29,7 30,6 V 4 L 29,3 28,4 V 2 L 27,1 26,0 h -2 z m 2,20 1,1 1,1 1,1 1,1 H 26 L 25,23 24,22 Z M 8,22 h 2 l -1,1 -1,1 z"\n\t/>\n<path class="fill2" id="fill2" style="fill:#407482;"\n\td="m 18,8 1,1 1,1 1,-1 -1,-1 z M 8,10 7,11 6,12 h 2 2 l -1,1 -1,1 h 2 v 2 h 2 v 2 l 1,-1 1,-1 -1,-1 -1,-1 h 2 l -1,-1 -1,-1 -1,-1 1,-1 h -2 z m 4,8 -1,-1 -1,-1 H 8 L 7,17 6,18 H 4 l -1,1 -1,1 v 2 1 1 2 l 1,1 1,1 H 6 L 5,27 4,26 H 6 L 5,25 6,24 v -2 l 1,-1 1,-1 h 2 l 1,-1 z M 8,14 7,13 6,12 H 4 2 v 2 h 2 2 z m 10,-2 -1,1 -1,1 v 2 l -1,1 -1,1 h 2 l 1,-1 1,-1 -1,-1 1,-1 1,1 1,1 v -2 l 1,1 -1,1 1,1 -1,1 h 2 v -2 -2 l -1,-1 -1,-1 z m 4,6 -1,1 -1,1 1,1 -1,1 -1,1 1,1 1,-1 1,-1 v -2 z m -2,0 -1,-1 -1,1 1,1 z m 8,-2 -1,1 -1,1 v 2 l 1,-1 1,-1 1,-1 z m -14,4 -1,1 -1,1 v 2 l 1,1 1,1 1,1 1,1 h 2 v -2 -2 l -1,-1 -1,-1 -1,-1 z m 14,0 -1,1 1,1 1,1 1,-1 -1,-1 z m -5,3 -1,1 -1,1 1,1 1,1 1,1 h 2 2 l 1,-1 1,-1 v -2 l -1,1 -1,1 h -2 l -1,-1 -1,-1 z m -13,1 -1,1 -1,1 v 2 l 1,1 1,-1 -1,-1 1,-1 1,-1 z m 0,2 1,1 1,-1 z m 2,2 1,1 1,-1 z m 7,1 -1,1 1,1 1,-1 z m -1,1 h -2 l 1,1 z"\n\t/>\n<path class="out2" style="fill:#27464f;"\n\td="m 16,6 1,1 1,1 h 2 l 1,1 -1,1 -1,-1 -1,-1 h -2 l 1,1 1,1 -1,1 -1,1 h 2 2 l 1,1 1,1 v 2 2 2 2 l -1,1 -1,1 -1,-1 -1,-1 -1,-1 1,-1 v -2 l 1,-1 -1,-1 -1,1 -1,1 h -2 l 1,-1 1,-1 h -2 l -1,1 -1,1 -1,1 -1,1 H 8 l -1,1 -1,1 v 2 l 1,1 -1,1 v 2 H 4 L 3,27 2,26 V 24 22 20 L 3,19 4,18 H 2 l -1,1 -1,1 v 2 2 2 l 1,1 1,1 1,1 1,1 h 2 2 2 2 2 l 1,1 1,1 h 2 2 l 1,-1 1,-1 v -2 l -1,1 -1,-1 v -2 l 1,1 1,1 1,1 1,1 h 2 2 l 1,-1 1,-1 1,-1 1,-1 v -2 -2 l -1,-1 -1,-1 -1,-1 1,-1 v -2 l -1,-1 -1,-1 -1,1 -1,1 -1,1 -1,1 V 16 14 L 23,13 22,12 V 10 8 L 21,7 20,6 H 18 Z M 16,6 15,7 14,8 13,9 12,8 H 10 8 L 7,9 6,10 H 4 2 0 v 2 h 2 2 2 l 1,-1 1,-1 h 2 2 l -1,1 1,1 1,-1 1,-1 1,-1 1,1 V 8 Z m 0,4 -1,1 -1,1 -1,1 1,1 1,-1 1,-1 z m -2,4 1,1 1,1 V 14 Z M 0,12 v 2 l 1,1 1,1 H 4 L 3,15 2,14 1,13 Z m 4,4 h 2 l -1,1 -1,1 h 2 l 1,-1 1,-1 h 2 V 14 H 8 6 4 Z m 14,0 1,-1 -1,-1 -1,1 z m 10,0 1,1 -1,1 -1,1 -1,1 v -2 l 1,-1 z m -14,4 1,1 1,1 1,1 1,1 v 2 2 h -2 l -1,-1 -1,-1 -1,-1 -1,-1 v -2 l 1,-1 z m 10,0 1,1 1,1 1,1 1,1 h -2 l -1,-1 -1,-1 z m 4,0 1,1 1,1 -1,1 -1,-1 -1,-1 z M 8,22 h 2 l -1,1 -1,1 z m 15,1 1,1 1,1 1,1 h 2 l 1,-1 1,-1 v 2 l -1,1 -1,1 h -2 -2 l -1,-1 -1,-1 -1,-1 1,-1 z m -13,1 1,1 1,1 1,1 1,1 -1,1 -1,-1 -1,-1 -1,-1 -1,1 1,1 -1,1 -1,-1 v -2 l 1,-1 z m 9,5 1,1 -1,1 -1,-1 -1,1 -1,-1 h 2 z"\n\t/>\n<path class="fill1" id="se1" style="fill:#6ff7fa;"\n\td="m 22,0 1,1 1,1 1,1 -1,1 v 2 2 2 l 1,1 1,1 v 2 l 1,1 1,-1 V 12 L 27,11 26,10 V 8 6 l 1,1 1,1 1,-1 1,-1 V 4 L 29,3 28,4 V 2 L 27,1 26,0 h -2 z m 6,8 v 2 l 1,1 1,1 1,-1 -1,-1 1,-1 1,-1 h -2 l -1,1 z m 2,4 -1,1 1,1 1,-1 z m 0,2 -1,1 1,1 1,-1 z M 16,10 v 2 l 1,-1 1,-1 z m 0,2 -1,1 -1,1 h 2 l 1,-1 1,-1 z m -2,2 h -2 l 1,1 1,1 h 2 L 15,15 Z M 2,16 1,17 0,18 h 2 l 1,-1 1,1 1,-1 1,-1 H 4 Z m 8,0 1,1 1,1 v -2 z"\n\t/>\n<path class="out1" id="se2" style="fill:#3791c4;"\n\td="m 26,6 v 2 2 l 1,1 1,1 v 2 l 1,1 1,-1 -1,-1 1,-1 -1,-1 -1,-1 V 8 L 27,7 Z m -13,5 -1,1 1,1 1,-1 z m -13,1 1,1 1,1 v -2 z"\n\t/>\n<path class="teeth1" style="fill:#e1e9ff;"\n\td="m 2,14 1,1 1,1 v -1.999989 z"\n\t/>\n<path class="orn2" id="mark2" style="fill:#4bfb49;"\n\td="m 15,9 -1,1 h 2 z m 4,10 -2,2 1,1 h 2 l -1,-1 1,-1 z m -7,7 v 2 l 1,-1 z"\n\t/>\n<path class="orn1" id="mark1" style="fill:#ff46ba;"\n\td="m 14,10 -1,1 1,1 2,-2 z m 6,4 v 2 l 1,-1 z m 0,2 -1,-1 -1,1 2,2 1,-1 z m 0,2 -1,1 1,1 2,-2 z m 0,2 -1,1 1,1 1,-1 z m 0,2 h -2 l 1,1 z m -1,-3 -1,-1 v 2 z m -13,5 -1,1 1,1 1,-1 z m 0,2 H 4 l 2,2 z m 5,-1 -1,1 h 2 z m 1,1 -1,1 1,1 z m 0,2 h 2 l -1,-1 z"\n\t/>\n<path class="eyemid1" style="fill:#f5f8ff;"\n\td="M 8,12 H 6 l 1,1 z"\n\t/>\n<path class="eyes1" style="fill:#8696fb;"\n\td="m 8,12 -1,1 1,1 1,-1 1,-1 z"\n\t/>\n</svg>'



}