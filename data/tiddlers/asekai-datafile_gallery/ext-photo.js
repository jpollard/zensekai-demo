/*\
jedit: :folding=explicit:
created: 20190331105055160
tags: $:/asekai/datafile/gallery
title: $:/asekai/datafile/gallery/ext-photo.js
type: application/javascript
module-type: macro

ext photo

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

/* Information about this macro */
exports.name = "ext-photo";
exports.params = [ {name: "title"}, {name: "mode"} ];


var picUrl = function(dir, url) {
	if (/\/$/.test(dir) === false) {
		dir += '/';
	}
	
	return dir + url;
};

/* Run the macro */
exports.run = function(title, mode) {
	var tid, url, browserDir, nodeDir, dir, result = 'Error';
	
	mode = ( mode !== undefined && mode !== '' ? ' mode="' + mode + '"' : '' );
	
	// try to see if there's a tid included with nodejs
	nodeDir = $tw.wiki.getTextReference('$:/asekai/datafile/gallery/settings/node-dir', './');
	url = picUrl(nodeDir, title);
	tid = $tw.wiki.getTiddler(url);
	
	// if not just use the title
	if (tid === undefined) {
		url = title;
		tid = $tw.wiki.getTiddler(url);
		console.log('extphoto', url);
	}
	
	
	if (tid === undefined) {
		return '<figure>[ext photo not found]</figure>';
	}
	else if (/^image\//.test(tid.fields.type) === true) {
		result = '<$transclude tiddler="' + url + '"' + mode + ' />';
	}
	else {
		console.log('extphoto: ASSUMING BROWSER');
		
		// if a url is explicitly specified it can be used
		url = tid.fields.url;
		
		// or, you can put in the wiseguy value of url = true
		// to use the title as the url
		if (url === "true") {
			browserDir = $tw.wiki.getTextReference('$:/asekai/datafile/gallery/settings/browser-dir', './');
			url = picUrl(browserDir, title);
		}
		
		result = '<img src="' + url + '" alt="' + url + '" />';
		
		
		// on nodejs.... there's an extra headache to this.
		// images don't like to load from disk, so you must import them as tids
		// this leads to there being 1 image tid, 1 meta tid, & 1 l10n tid.
		// it's dumb, yet still less error-prone than tw's native lazy loading.
		// which would not even work when i tried it.
		
			
	}
	
	console.log('EXT PHOTO:', result);
	return result;
};

})();
