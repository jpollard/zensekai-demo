author: Takumi
core-version: >=5.0.0
created: 20190308115704917
description: Asekai basic language strings
list: readme all
plugin-type: kai-lang
tags: remove_in_min l10n
title: $:/asekai/language/en/basic
type: application/hjson
version: 19.03.27

{

readme: 
"asekai english language strings"

"$:/asekai/language/en/basic/all": {
	"tags": "$:/asekai/language/en/basic"
	"text": "All tids (English generic):\n\n<ul>\n \n<$list filter=\"[all[tiddlers+shadows]prefix[$:/asekai/lang/en/]]\">\n<li>{{!!title}}</li>\n</$list>\n\n</ul>"
}

language: {
	"name-space": " "
	"name-order": "an"
	"pron-space": " "
	"pron-phonetic": "no"
}



"sectors/default": {
	"name": "Default"
}

"sectors/nature": {
	"name": "Nature"
}

"sectors/culture": {
	"name": "Culture"
}

"sectors/spirit": {
	"name": "Spirit"
}

"sectors/physics": {
	"name": "Physics"
}



"sorts/dfcf": {
	"name": "Peak"
}

"sorts/dfcp": {
	"name": "Brink"
}

"sorts/dfep": {
	"name": "Verge"
}

"sorts/dfef": {
	"name": "Flare"
}

"sorts/dpcf": {
	"name": "Vale"
}

"sorts/dpcp": {
	"name": "Holt"
}

"sorts/dpep": {
	"name": "Luke"
}

"sorts/dpef": {
	"name": "Spring"
}

"sorts/npcf": {
	"name": "Sperryl"
}

"sorts/npcp": {
	"name": "Bloom"
}

"sorts/npep": {
	"name": "Mare"
}

"sorts/npef": {
	"name": "Arc"
}

"sorts/nfcf": {
	"name": "Wolfram"
}

"sorts/nfcp": {
	"name": "Lode"
}

"sorts/nfep": {
	"name": "Firth"
}

"sorts/nfef": {
	"name": "Nadir"
}


"sorts/char": {
	"name": "Glacial"
}

"sorts/synk": {
	"name": "Inferno"
}

"sorts/sass": {
	"name": "Abyss"
}

"sorts/maya": {
	"name": "Terrace"
}

"sorts/core": {
	"name": "???"
}



//"variants/dfcf": {}

"variants/breeze": {
	"name": "Breeze"
	"name-pron": "bɹiːz"
}

"variants/millennium": {
	"name": "Millennium"
	"name-pron": "mɪˈlɛnɪəm"
}

"variants/celestial": {
	"name": "Celestial"
	"name-pron": "səˈlɛstiəl"
}

//"variants/dfcf": {}

"variants/technic": {
	"name": "Technic"
	"name-pron": "ˈtɛk.nɪk"
}

"variants/tall": {
	"name": "Tall"
	"name-pron": "tɔl"
}

"variants/metric": {
	"name": "Metric"
	"name-pron": "ˈmɛt.ɹɪk"
}

"variants/primeval": {
	"name": "Primeval"
	"name-pron": "ˈpɹaɪˌmi.vəl"
}

//"variants/npcf": {}

"variants/chrome": {
	"name": "Chrome"
	"name-pron": "kɹoʊm"
}

"variants/liminal": {
	"name": "Liminal"
	"name-pron": "ˈlɪmɪnəl"
}

//"variants/npef": {}

//"variants/nfcf": {}

"variants/course": {
	"name": "Course"
	"name-pron": "kɔɹs"
}

"variants/paneer": {
	"name": "Yore"
	"name-pron": "jɔɹ"
}

//"variants/nfcf": {}

"variants/youth": {
	"name": "Youth"
	"name-pron": "juθ"
}

"variants/great": {
	name: "Great"
	"name-pron": "ɡɹeɪt"
}


"variants/youth": {
	"name": "Youth"
	"name-pron": "juθ"
}


"variants/char": {
	"name": "Shade"
	"name-pron": "ʃeɪd"
}

"variants/synk": {
	"name": "Heck"
	"name-pron": "hɛk"
}

"variants/sass": {
	"name": "OutThere"
	"name-pron": "aʊt.ðɛɚ"
}

"variants/char2": {
	"name": "Funkified"
	"name-pron": "ˈfʌŋkɪfaɪd"
}


"variants/skyblue": {
	"name": "Sky"
	"name-pron": "skaɪ"
}
"variants/glacierblue": {
	"name": "Frost"
	"name-pron": "fɹɑst"
}





"$:/asekai/lang/en/tw-ui/kai-pronunciation": { text:
	"pronunciation"
}


"$:/language/Docs/Fields/align-shift": { text:
	"[ASEKAI] \"''Shift''\" alignment of a kai from 1 (Day) to 8 (Night)"
}

"$:/language/Docs/Fields/align-phase": { text:
	"[ASEKAI] \"''Phase''\" alignment of a kai from 1 (\"change\"/\"full\"/\"Class\") to 8 (\"exist\"/\"new\"/\"Eris\")"
}

"$:/language/Docs/Fields/align-sort": { text:
	"[ASEKAI] \"''Sort''\" alignment of a kai, usually based on ''shift'' and ''phase''"
}


"$:/language/Docs/Fields/name-pron": { text:
	"[ASEKAI] Pronunciation of a kai's name using the International Phonetic Alphabet. Can hold a stringified array/object but if so should be JSON parsable when seen outside of an hjson file"
}

"$:/language/Docs/Fields/name-meaning": { text:
	"[ASEKAI] Concise meaning of a kai's name, leaning toward in-universe-friendliness"
}

"$:/language/Docs/Fields/name-etym": { text:
	"[ASEKAI] Etymology of a kai's name, with meanings in current language"
}


"$:/language/Docs/Fields/num-sector": { text:
	"[ASEKAI] Number of sector a kai originates from/is associated with"
}

"$:/language/Docs/Fields/num-index": { text:
	"[ASEKAI] Number of a kai strain within its sector, e.g. 01-000''3''. Should not be zerofilled"
}

"$:/language/Docs/Fields/num-serial": { text:
	"[ASEKAI] Serial number tracking the (approximate) order kai strains were made in. Optional; if used, should start at 0 for a given author"
}

"$:/language/Docs/Fields/num-sorting": { text:
	"[ASEKAI] Computed sorting string for kai forms, based on strain's numbers and meta stage"
}



}