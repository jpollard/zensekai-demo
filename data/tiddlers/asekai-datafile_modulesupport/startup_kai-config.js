/*\
jedit: :folding=explicit:
created: 20190323063644616
tags: $:/asekai/datafile/kai-strain
title: $:/asekai/datafile/startup/kai-config.js
type: application/javascript
module-type: startup

read $:/asekai/config

\*/
(function(){
/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var heart = require('$:/asekai/datafile/lib/heart.js');
var Hjson = require('$:/plugins/laktak/hjson/hjson.js');

// Export name and synchronous status
// $:/core/modules/startup/kai-strain.js
exports.name = "kai-config";
exports.before = ["startup"];
exports.after = ["load-modules"];
exports.synchronous = true;


exports.startup = function() {
	var wiki = $tw.wiki, i,
	    configTid, cfgTidFields, unpacked = {};
	const p = '$:/asekai/config/';
	
	try {
		configTid = wiki.getTiddler('$:/asekai/config');
		
		if (configTid === null) {
			return;
		}
		else {
			cfgTidFields = configTid.fields;
			configTid = Hjson.parse(configTid.fields.text);
		}
		
		//onsole.log('config tid', configTid);
		
		if (configTid['current-language'] !== undefined &&
		  wiki.tiddlerExists(p + 'current-language') !== true)
		{
			unpacked[p + 'current-language'] =
			configTid['current-language'];
		}
		
		// create tids
		for (i in unpacked) {
			unpacked[i] = {
				title: i,
				tags: '$:/asekai/config',
				text: unpacked[i],
				created: cfgTidFields.created,
				modified: cfgTidFields.modified,
			};
			
			$tw.wiki.addShadowTiddler(unpacked[i], '$:/asekai/config');
		}
		
		$tw.wiki.unpackPluginTiddlers();
	}
	catch (e) {
		console.log('error in config', e);
	}
	
};

})();
