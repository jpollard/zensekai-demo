
**zensekai** ("whole world" + "Zen world" + "monster") is intended to be a lightweight version of "Asekai" for linux smartphones — that is, an AR "[mon](https://allthetropes.org/wiki/Mon)" simulation game built on the [Godot](https://godotengine.org/) engine.

What's working in the demo (not much right now):

* Moving around manually (use arrow keys)
* Finding "MapKai" — which technically have a real model now
* Generating a new spawn grid when you walk far enough
* Clicking on MapKai — no visible effect right now.


#### What should I do with this demo?

First get one of the [Releases](https://codeberg.org/Valenoern/zensekai-demo/releases). Then:

1. To run the demo, start the `zensekai` script in your release
2. If you want to look closer at the 3d engine, install the Godot editor

(For more detailled information including installing Godot, troubleshooting, compiling, etc., check the manual in the `docs` folder.  
The zensekai manual is also online at [pages.codeberg.org](https://pages.codeberg.org/Valenoern/zensekai/docs/docs.html).)

After that... I don't know. If you have any bugs you don't think I've found, or anything else to say, go find me on [mastodon](https://floss.social/@Valenoern) or [twitter](https://twitter.com/Valenoern).  
Already-known issues can be found on this project's [Issues tab](https://codeberg.org/Valenoern/zensekai-demo/issues).

The docs will eventually contain information on how to use the project's various development tools (tiddlywiki, rustc, blender, etc). However at this stage, the demo can *mostly* be inspected with the Godot editor.

At this time the demo has been tested on devuan ascii and windows 10.


---

### About Asekai

**asekai** ("aesthetic" + "monster" + "a world") is a simulation game inspired by [Monster Rancher](https://monster-rancher.fandom.com/wiki/Monster_Rancher_Wiki) (Monster Farm) and [Digimon](https://wikimon.net/Digimon), among other things. Players select one of 16 characters on the "Sort grid" and take on a Quest specifically suited to their companion "Kai" and the "sector" of the world they live in to try to win the circuit and/or actualise their companion(s) to their final form, along the way running into other "grid characters" and many, *many* different Kai forms.

The game is in heavy development, and zensekai is actually being made first as a way to lay down the central code for both game versions.

Check out my mastodon feed for more information and news about the game.


### misc

all asekai versions (asekai, zensekai, etc.) are "formally" released under the [MIT var. X11](https://directory.fsf.org/wiki/License:X11)

however, the "LICENSE" file should never be taken as limiting your basic rights to appreciate the work. please see the "INTENT" file for more information on this.

note: there's currently a possibility on the table for the game to move to the [GPL3](https://www.gnu.org/licenses/gpl-3.0.en.html) by the time the base content is complete, but this isn't certain.