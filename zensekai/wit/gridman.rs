// GridMan - a program to generate and manage spawn grids
// :folding=explicit: <- jedit prefs {{{

// headers {{{ 
extern crate rand;
extern crate pcg_rand;

use std::io::{Error, ErrorKind}; // Read
use std::time::{SystemTime};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::env::args;
use rand::{Rng, SeedableRng};
use pcg_rand::{Pcg32}; // seeds::PcgSeeder
// }}}

// constants
const SPAWNGRID_SIZE : u8 = 16;
const LOW_DENSITY : f64 = 0.305;
// use a spawn cycle length of 10 mins = 60 sec * 10
const CYCLE_LENGTH : u16 = 600;
//const CYCLE_LENGTH : u16 = 1;


// SpawnGrid {{{
pub struct SpawnGrid { // {{{
	// list contains a simple vector of grid objects
	list: Vec<GridObj>,
	// then grid simply holds a contiguous map of what is at what cell
	// u16 can hold up to nearly 256*256 cells
	// more than adequate for a 16*16 grid
	grid: Vec<Option<u16>>,
	// size of grid
	size: u8
} // }}}

impl SpawnGrid {
	// constructor
	pub fn new() -> Self { // {{{
		let sgsize = SPAWNGRID_SIZE as u16;
		// specifying a vector size requires a usize
		let cells = sgsize.pow(2) as usize;
		
		// allocate enough space vector will not need to expand,
		// but leave elements empty
		let list = Vec::with_capacity(cells);
		// leave a dummy cell at 0,
		// so "0" can be used to mean "cell empty, don't look it up"
		//list.push( GridObj::empty() );
		
		Self {
			list: list,
			// grid is less complicated and can simply be filled in
			grid: vec![None; cells],
			size: SPAWNGRID_SIZE
		}
	} // }}}
	
	// retrieve empty or full cell at given coordinate
	pub fn cell_at(&self, x : u8, y : u8) -> Option<&GridObj> {
		let mut cell : Option<&GridObj> = None;
		
		let cell_id : usize = self.cell_id(x, y);
		let list_item = &self.grid[cell_id];
		
		// grid uses Option to represent if a cell is filled or not
		// if it contains a GridObj this function returns Some(GridObj)
		// otherwise it returns None
		if list_item.is_some() {
			let list_item_ref = list_item.unwrap() as usize;
			let cell_contents = &self.list[ list_item_ref ];
			cell = Some(cell_contents);
		}
		
		return cell;
	}
	
	pub fn cell_is_filled(&self, x : u8, y : u8) -> bool { // {{{
		let cell_id : usize = self.cell_id(x, y);
		let list_item = &self.grid[cell_id];
		
		if list_item.is_some() {
			return true;
		}
		
		return false;
	} // }}}
	
	pub fn put_cell(&mut self, item : GridObj, x : u8, y : u8) -> Result<(), Error> {
		let cell_id : usize = self.cell_id(x, y);
		
		// item will be added to end of list,
		// so get its ID by getting list's length
		self.list.push(item);
		let item_id = self.list.len() as u16 - 1;
		
		self.grid[cell_id] = Some(item_id);
		
		Ok(())
	}
	
	pub fn clear_cell(&mut self, x : u8, y : u8) -> Result<(), Error> {
		let cell_id : usize = self.cell_id(x, y);
		let item_id = self.grid[cell_id].unwrap() as usize;
		
		self.grid[cell_id] = None;
		// the item is not actually removed
		// given that it would cause the item ids to change
		self.list[item_id] = GridObj::empty();
		
		Ok(())
	}
	
	// convert coordinates to cell id
	// this is private in case the implementation changes
	fn cell_id(&self, x: u8, y: u8) -> usize { // {{{
		let x_16 = x as u16;
		let y_16 = y as u16;
		let size = self.size as u16;
		
		// ooo ooo o   o   ...
		// y=0 y=1 x=0 x=1 ...
		let cell_id = (y_16 * (size - 1)) + x_16;
		
		return cell_id as usize;
	} // }}}

	pub fn to_chars(&self) -> String { // {{{
		let square_empty = "░░".to_string();
		let square_full = "██".to_string();
		
		let size = self.size;
		let size_ref = self.size as usize;
		let string_size : usize = self.grid.len() + (size_ref * 2);
		let mut chars : Vec<String> = Vec::with_capacity( string_size );
		
		for x in 0..size {
			for y in 0..size {
				if self.cell_is_filled(x, y) {
					chars.push(square_full.clone());
				}
				else {
					chars.push(square_empty.clone());
				}
			}
			chars.push("\n".to_string());
		}
		
		return chars.join("");
	} // }}}
	
	pub fn to_json(&self) -> String { // {{{
		let size = self.size;
		let size_ref = self.size as usize;
		let string_size : usize = self.grid.len() + (size_ref * 2);
		let mut chars : Vec<String> = Vec::with_capacity( string_size );
		
		chars.push("{ \"positions\": [ ".to_string());
		
		for x in 0..size {
			if x > 0 {
				chars.push(", [ ".to_string());
			}
			else {
				chars.push("[ ".to_string());
			}
			
			for y in 0..size {
				if y > 0 {
					chars.push(", ".to_string());
				}
				
				if self.cell_is_filled(x, y) {
					chars.push("1".to_string());
				}
				else {
					chars.push("0".to_string());
				}
			}
			
			chars.push(" ] ".to_string()); // \n
		}
		
		chars.push("] }".to_string());
		
		return chars.join("");
	} // }}}
	
} // }}}

pub struct GridObj {
	has_kai: bool,
	kai_ident: String
}

impl GridObj {
	pub fn empty() -> Self {
		Self {
			has_kai: false,
			kai_ident: "".to_string()
		}
	}
}

// }}}


// obtain prng seed based on current time
fn create_seed() -> Result<u64, Error> { // {{{
	create_seed_salted("".to_string())
} // }}}

// create seed given some arbitrary 'name' of a grid block as a salt
// so that two different grids can be generated during the same cycle,
// but generating a grid with the SAME name still gives same result
fn create_seed_salted(salt: String) -> Result<u64, Error> { // {{{
	// within a "spawn cycle", say 10 min (600 sec),
	// the results for a given grid should be the same the whole time
	// so the same seed is used until the cycle rolls over
	let cycle_length = CYCLE_LENGTH as u64;
	
	// u64 is *huge* for the cycle length,
	// but if we must pick 1 type (we must.), very good for unix time
	// right now unix time is at almost half a u32 of seconds
	// however it will take over a million years (!!) to exhaust u64
	
	// getting the time is mildly complicated
	// you must first get an 'instant'
	let now = SystemTime::now();
	// then find the difference between another time
	let now_numeric = now.duration_since(SystemTime::UNIX_EPOCH);
	// unpack a Duration { sec: seconds, nsec: extra nanoseconds }
	// TODO: don't unwrap maybe
	let current_time : u64 = now_numeric.unwrap().as_secs();
	
	// round down the time to 'number of cycles' (10 min) represented
	// doing an integer division rounds down but this is fine
	let cycles = current_time / cycle_length;
	
	// hash the string and cycle count in order to combine them
	// hasher actually returns a plain u64 (??) it seems
	let mut hasher = DefaultHasher::new();
	hasher.write_u64(cycles);
	hasher.write(salt.as_bytes());
	let seed : u64 = hasher.finish();
	//rintln!("Hash is {:x}", salt_hash);
	
	Ok(seed)
} // }}}

// generate spawn grid
fn output_grid() -> Result<SpawnGrid, Error> { // {{{
	output_grid_called("".to_string())
} // }}}

fn output_grid_called(salt: String) -> Result<SpawnGrid, Error> { // {{{
	// number of grid cells per side
	let sgsize = SPAWNGRID_SIZE as f64;
	let sg_max = SPAWNGRID_SIZE - 1;
	// population = grid size ^ 2 * density
	// at SPAWNGRID_SIZE of 16 population is 78
	let population = (sgsize.powi(2) * LOW_DENSITY) as u16;
	
	// get prng seed based on time
	let seed : u64 = create_seed_salted(salt).unwrap();
	
	// initialise prng (pcg = permuted congruential generator)
	// pcg_rand requires a bunch of "poison for cuzco cuzco's poison" idioms
	// but that may be about how rust is in general
	//let mut pcg = Pcg32::from_seed(PcgSeeder::seed(seed));
	
	// seed_from_u64 is "undocumented" (may change) but distinctly shorter
	let mut pcg = Pcg32::seed_from_u64(seed);
	
	// create grid
	let mut grid = SpawnGrid::new();
	
	// place a kai according to 'population' number
	for _pop_placed in 1..population {
		let x = pcg.gen_range(0, sg_max);
		let y = pcg.gen_range(0, sg_max);
		
		if grid.cell_is_filled(x, y) == false {
			let kai = GridObj {
				has_kai: true, kai_ident: "".to_string()
			};
			
			let cell_put = grid.put_cell(kai, x, y);
			
			if cell_put.is_err() {
				//handle error
			}
		}
	}
	
	Ok(grid)
} // }}}


// get first string given as argument - `./test "asdf"`
fn open_first_string() -> Result<String, Error> { // {{{
	let args: Vec<String> = std::env::args().collect();
	let has_arg: usize = args.len();
	
	// if there isn't a first argument, stop & return an error
	if has_arg < 2 {
		return Err(Error::new(ErrorKind::Other, "no arg"));
	}
	
	// get first argument as a reference
	let _raw = &args[1];
	// in order to dereference a reference you have to "clone" the value
	Ok(_raw.clone())
} // }}}

fn main() -> Result<(), Error> { // {{{
	let first_arg = open_first_string();
	
	let salt = match first_arg.is_ok() {
		true => first_arg.unwrap(),
		_ => "".to_string()
	};
	
	let grid : SpawnGrid = output_grid_called(salt).unwrap();
	let grid_string = grid.to_json();
	//let grid_string = grid.to_chars();
	
	println!("{}", grid_string);
	
    Ok(())
} // }}}


/* NOTES {{{

	a "reasonable" maximum polygon to expect a user to traverse in a day,
	assuming i am a "reasonable" user, is:
	
	4381.79 m  *  2297.23 m
	- about 3.886496374184 square miles
	
	
	KAI DENSITY
	
	kai are said to populate the world at a rate of 4 kai : 1 human
	
	estimated pop. around sample rect:
	= 724 / mi^2  = ~2814 people  = 11,256 kai
	
	estimated pop. of new york:
	27000 / mi^2  = ~104935 people  = 419,741 kai
	
	
	4300 * 2200 =  9460000  - actually too many
	4300/16 * 2200/16 =   36953.125
	4300/4  * 2200/4  =  591250
	
	a 4000 x 2000 meter rect -> 1000 x 500 grid, 500,000 cells
	if each cell is one char, that is 500 kb of memory.
	i.e. if you pick up 4 of these polygons, only 2 mb !
	
	data for what is actually IN cells will take more than a char
	but there will almost certainly not be as many as cells
	and can be emptied if their data goes elsewhere - bag, contacts, etc
	
	also the whole spawn grid will be emptied after ~10 minutes
	you will Probably not rack up over 10 mb of grid rectangles in that time
	
	
	DENSITY
	
	0.30460211416 kai for cells @ 16-meter cell nature?sector density
	0.70992135306 kai for cells @  4-meter cell culturesector density

}}} */