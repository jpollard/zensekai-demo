#!/bin/bash
release=./target/release
cd "${0%/*}" # build in dir of build.sh

# for testing program
test() {
cargo build && ./target/debug/gridman "$1"
}

# for building clean binary; used by make
release() {
rm ../bin/gridman
cargo build --release && \
cp $release/gridman ../bin/
}

if [ "$1" == "release" ]; then
  release
else
  test
fi
