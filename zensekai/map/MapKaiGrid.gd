extends Spatial
class_name MapKaiGrid
# script containing 'library functions'
const maplib = preload("map.gd")
# :folding=explicit: <- jedit prefs

# TODO: test how windows likes this new system.....
#var gridManPath = {
#	"Windows": ".\\bin\\gridman.exe",
#	"defaultOS": "./bin/gridman"
#}
var binPath = maplib.bin_path()
var gridManPath = binPath + "gridman"

# see _init etc.
# /why/ can't i just chain these together with commas. this isnt C where the vars are types
var origin; var overlay; var collision; var models; var seeder

signal leader_exited_grid(body, origin)


# if the leader leaves this grid, notify MapKaiLair
# to make/reactivate new grid
func _on_body_exited(body):
	emit_signal("leader_exited_grid", body, get_translation())
#

# disappear or (re)appear kai meshes on this grid
func toggle_kai(mode): # {{{
	var body
	var children
	
	children = self.get_children()
	
	for body in children:
		# don't dematerialise the plane, only MapKai
		if body.get_class() == "StaticBody" and mode == "unload":
			body.unload()
		elif body.get_class() == "StaticBody" and mode == "reload":
			body.reload()
	#

func unload_kai():
	toggle_kai("unload")

func reload_kai():
	toggle_kai("reload")
# }}}

# Called when the node enters the scene tree for the first time.
func _ready(): # {{{
	# global origin, seeder
	
	# create nodes/mesh/collision data for grid
	grid_mesh(origin)
	
	# pass the "name" of the grid so each grid is different
	var salt = grid_at(origin)
	# fetch spawn grid data
	var spawnGrid = generate_grid(salt)
	
	# if there are no problems, use grid
	if spawnGrid.okay == true:
		# seeder is used to randomise kai models within their grid cell
		seeder.randomize()
		
		# make grid data into objects/nodes
		place_objects(spawnGrid.positions)
# }}}

# generate grid data with external "GridMan" program
func generate_grid(salt): # {{{
	var spawnGrid

	# compose command to run
	# this will generate output in output array.
	var gridManCommand = [ gridManPath, " \"" + salt + "\"" ]
	var output = []
	var exit = maplib.execute_with_output( gridManCommand, output )
	
	# in GridMan the time & "salt" are combined to create the seed
	
	# initially I just made gridAttempt like { positions: [...] }
	# for valid JSON, but to move the whole GridMan call into here,
	# I ended up using gridAttempt a lot like a rust Option<Some, None>
	if exit == 0:
		var gridAttempt = JSON.parse(output[0])
		spawnGrid = gridAttempt.result
		spawnGrid.okay = true
		#print("SPAWNGRID: ", spawnGrid)	
	else:
		print("GRID OUTPUT: ", output)
		print("could not get new grid data from ", gridManPath, " - exit ", exit)
		print("command:", gridManCommand)
		spawnGrid = { "okay": false }
	
	return spawnGrid
# }}}

# create nodes/mesh/collision data for grid
func grid_mesh(origin): # {{{
	var models = self.models
	var overlay = self.overlay
	var gridSize = 16
	var gridFullSize = gridSize * gridSize
	
	# move to origin, converted from grid units to meters
	var originTrue = Vector3(origin.x * gridFullSize, origin.y * gridFullSize, origin.z * gridFullSize)
	translate(originTrue)

	# retrieve grid cell mesh & copy it as the mesh of current MapKaiGrid
	# i at first thought you had to duplicate meshes but you dont
	# you can just set_mesh an *existing* mesh and don't have to
	var mesh = models.get_node("GridPlane").get_mesh()
	
	# overlay is just a container for the mesh
	overlay.set_mesh(mesh)
	# create collision area
	collision_box()
	# mesh & collision are stored in the 'Area' for tidiness
	collision.add_child(overlay)
# }}}

# create collision boundaries for whole grid
func collision_box(): # {{{
	# global collision

	# it appears to be best to actually create a new collision object
	var collisionBox = CollisionShape.new()
	# CollisionShapes only manage collision-related methods;
	# collisionData contains the actual collision solid
	var collisionData = BoxShape.new()
	# specifies a box with a 128x128 floor & 32 height
	# TODO: what were the maths for this
	collisionData.set_extents(Vector3(128,32,128))
	collisionBox.set_shape(collisionData)
	collisionBox.set_disabled(false)
	
	# "collision" is an Area, which detects objects /inside/ it
	collision.set_ray_pickable(false)  # avoid grids disabling clicks
	collision.add_child(collisionBox)
	add_child(collision)
	
	collision.connect("body_exited", self, "_on_body_exited")
	enable_leave()
#

# turn actual collision checks on or off
func enable_leave():
	collision.set_collision_layer(8) #1000 - is nothing
	collision.set_collision_mask(1) #0001 - seeks player

func disable_leave():
	collision.set_collision_layer(8) #1000 - is nothing
	collision.set_collision_mask(8) #1000 - seeks nothing
# }}}

# turn raw spawnGrid data into map objects
func place_objects(spawnGrid): # {{{
	var x
	var z
	var gridSize = spawnGrid[0].size()

	for x in range(gridSize):
		for z in range(gridSize):
			if spawnGrid[x][z] > 0:
				place_object(x, z)
# }}}

# given a coordinate place a MapKai
# LATER: later there may be a distinction between
# squares with kai and squares with items etc., hence "object"
func place_object(xpos, zpos): # {{{
	var origin = self.origin
	var models = self.models
	# this will be used to give the map object a unique ID
	var kaiPositions = get_child_count()
	# also append the name of this grid for debug
	var originStr = grid_at(origin)
	# the final name of the object will be like: KaiPos-30-1n_5e
	
	# give model by duplicating from model cache
	# for now, all kai use "entity_cube"
	# LATER: test out with like four differently coloured Entities
	var ident = "entity_entity"
	var model = agent_model(ident, models)
	var mapkai = MapKai.new(model)

	mapkai.set_name("KaiPos" + str(kaiPositions) + "-" + originStr)
	mapkai.translate(position_for(xpos, zpos))
	add_child(mapkai)
# }}}

# process leader/mapkai model
func agent_model(ident, models): # {{{
	var mode = "MapKai"
	var mesh
	var collision
	var scene
	ident = "entity_entity"
	
	if mode == "MapKai":
		var kaiRefs = models.get_node("KaiRefs")
		#mesh = kaiRefs.get_node("k_" + ident)
		scene = kaiRefs.get_node("k_" + ident).duplicate()
		
	#mesh = mesh.duplicate()
	# mesh node will be named by its strain so rename it
	#mesh.set_name("mesh")
	# child nodes are duplicated with a node so simply fetch collision
	#collision = mesh.get_node("collision")
	#mesh.remove_child(collision)
	
	#var model = { "ident": ident, "mesh": mesh, "collision": collision }
	var model = { "ident": ident, "scene": scene }
	#print("MODEL", ident, collision)
	
	# scale model to be more visible
	#model.scale_object_local( Vector3( 1.7 , 1.7 , 1.7 ) )
	
	return model
# }}}

# convert simple (x, z) coord in grid-cell units to messy godot vector
func position_for(x, z): # {{{
	var seeder = self.seeder
	var gridSize = 16.0
	var halfGrid = gridSize * (7.0/16.0)
	
	var trueX = true_coord(x, gridSize)
	var trueZ = true_coord(z, gridSize)
	
	# randomise positions slightly.
	trueX = trueX + seeder.randf_range(-1 * halfGrid, halfGrid)
	trueZ = trueZ + seeder.randf_range(-1 * halfGrid, halfGrid)
	
	return Vector3(trueX, 0, trueZ)

# convert simple 0~16 coords to meters; position_for helper
static func true_coord(coord, gridSize):
	var offset = (gridSize / 2) - 0.5
	return 0 + ((coord - offset) * gridSize)
# }}}

# calibrate this grid to sit at given "real" coordinates - unused
func set_origin(vector3d):
	self.origin = vector3d
	translate(vector3d)
#

# pass Models node - containing a model of each kai - into this script
func _init(origin, models):
	# origin of this grid in whole-grid units
	self.origin = origin
	# kai models
	self.models = models
	
	# containers for grid plane data
	self.overlay = MeshInstance.new()
	self.collision = Area.new()
	# will be used to adjust map objects
	self.seeder = RandomNumberGenerator.new()
#


# maplib imports {{{

# convert the integer coordinates of a grid to direction letters
func grid_at(vector3d):
	return maplib.grid_at(vector3d)

# for debug; just a funnily named nop function
func dont():
	pass

# }}}
