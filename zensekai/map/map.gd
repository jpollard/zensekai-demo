extends Spatial
# :folding=explicit: <- jedit prefs

onready var leader = $Leader
onready var mapKaiLair = $MapKaiLair

#var midclick = false

# when node enters, i.e. when scene 'enters'
func _ready():
	# answer MapKaiLair's signal zeroing_sabre with Leader's _on_scenery_zeroed
	# trying to fetch Leader or MapKaiLair from the other requires shenanigans,
	# so it's better done here
	mapKaiLair.connect("zeroing_sabre", leader, "_on_scenery_zeroed")
#

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#if Input.is_action_pressed("ui_click") and midclick == false:
	#	midclick = true
	#	print("click in loop")
	#	midclick = false


# 'library' functions to be used by classes in scene

# convert the integer coordinates of a grid to direction letters
# this creates much cleaner node IDs.
static func grid_at(vector3d): # {{{
	var directions = {
		"lat": { "-1": "w", "1": "e", "0": "" },
		"long": { "-1": "n", "1": "s", "0": "" }
	}
	
	var lats = directions.lat[ str(sign(vector3d.x)) ]
	var longs = directions.long[ str(sign(vector3d.z)) ]
	var latNo = str(abs(vector3d.x))
	var longNo = str(abs(vector3d.z))
	
	return "grid_" + latNo + lats + "_" + longNo + longs
# }}}

static func zero_vector():
	return Vector3(0,0,0)
#

# get what folder helper ("wit") programs are in
static func bin_path():
	var binpath = "./"
	var directory = Directory.new()
	var hasBin = directory.dir_exists("res://bin")
	
	if hasBin == true:
		binpath = "./bin/"
	
	return binpath

# wrap os.execute so windows can run commands.....
# Expects a "commandArray" like [ "./path", "arg1" ]
# sets output to output & returns simple exit value
static func execute(commandArray, output, shouldOutput): # {{{
	var exit = -1
	var os = OS.get_name() # example: "Windows", "X11"
	var blocking = true # 'true' executes process synchronously, blocking thread
	var argZero
	
	#if not os in commands:
	#	command = commands.defaultOS
	#else:
	#	command = commands[os]
	
	# by default, assume the os is "actually good" and can use unix-style paths
	# otherwise treat each exceptional case separately
	# this is necessary when ex. my devuan install is called "X11"
	if os == "Windows":
		# "fix" path slashes to ugly wrong escape-requiring ones
		commandArray[0] = commandArray[0].replace("/", "\\")
		# windows doesn't like to run commands
		# without them being passed through cmd.exe
		# which incidentally creates an ugly extra window, but whatever...
		argZero = "C:\\Windows\\System32\\CMD.exe"
		commandArray.push_front("/C")
	else:
		# godot requires the command's "$0" to be separate, so extract it
		argZero = commandArray.pop_front()
		# linux, & probably others, can simply run the command directly
	
	# finally, run the command
	exit = OS.execute( argZero, commandArray, blocking, output )
	
	if exit == -2:
		# godot, unlike any ordinary facility for running shell commands,
		# does not use 0 for a successful command but "-2".
		# this is actually so that it can return a process id - but only in async mode
		exit = 0
	
	# you can tell this function it "must" return output
	# if so it will give a nonzero exit on empty
	if shouldOutput == true && (output.size() == 0 || output[0].length() == 0):
		exit = 1
	
	return exit
#

# simpler wrapper to require output
static func execute_with_output(commands, output):
	var exit = execute(commands, output, true)
	return exit
# }}}
