build=./bin
arch=`uname -m`
armlist="_aarch64_" # not sure if this is the only possible arm string
template="Linux/X11 for x86_64"

if echo "$armlist" | grep -w "_${arch}_" > /dev/null; then
  template="Linux/X11 for arm64/pinephone"
fi

if [ ! -d "$build" ]; then
  mkdir "$build"
fi

echo "Exporting game with template: $template"
godot --no-window --export "$template" "$build/zensekai"
