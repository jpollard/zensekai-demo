#!/bin/bash
# script to perform `make clean`

# cd to dir above this script
#calleddir=$(cd "${0%/*}"; pwd)
cd "${0%/*}"; cd ..

for dir in ./bin ./build ./wit/target ./zensekai-click/godot ./zensekai-click/build; do
 if [ -d "$dir" ]; then
  rm -rf "$dir"
 fi
done
