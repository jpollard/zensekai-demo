#!/bin/bash

calleddir=$(cd "${0%/*}"; pwd)
cd "$calleddir"
platform=`./platform.sh`
launcher="zensekai"

#echo "$calleddir"

windowsver() {
 cp "./$launcher.bat" "../"
}

linuxver() {
 cp "./$launcher.sh" ./launcher_copy
 chmod u+x ./launcher_copy
 cp ./launcher_copy "../$launcher.sh"
 cp ./launcher_copy "../../$launcher.sh"

 if [ -f launcher_copy ]; then
  rm launcher_copy
 fi
}

if [[ "$platform" == "MSYS_NT" ]]; then
 windowsver
else
 if [[ "$platform" != "Linux" ]]; then
  echo "this seems not to be linux."
  echo "please edit the ./zensekai command to something sensible for your system"
 fi

 linuxver
fi

